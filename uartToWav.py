import serial
import wave
import numpy as np
import time
from scipy.signal import butter, lfilter
import os, fnmatch, shutil

def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(N=order, Wn=normal_cutoff, btype='highpass', analog=False)
    return b, a


def highpass_filter(f_audio_data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = lfilter(b, a, f_audio_data)
    return y


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y


#######################################################################################################################
# USER DEFINED READ PARAMETERS
#######################################################################################################################

# UART parameters
com_port = 'COM5'
baud_rate = 256000
data_bits = 8
stop_bit = 1

# Audio parameters
sample_rate = 16000
channels = 1

# Read parameters
read_timeout = 3

#######################################################################################################################
# BEGIN THE AUDIO RECEIVING PROCESS
#######################################################################################################################
while True:
    # Try to open serial port
    try:
        ser = serial.Serial(port=com_port, baudrate=baud_rate, bytesize=data_bits, stopbits=stop_bit)

        # Check if serial port is open
        if ser.isOpen():
            print(f"\nSuccessfully connected to {com_port}")
        else:
            print(f"Failed to connect to {com_port}")
            exit()

    except serial.SerialException as e:
        print(f"Error opening port: {str(e)}")
        exit()

    # Initialize buffer for data read
    audio_data = []
    data_count = 0

    # Initialize a time variable for tracking when the last data received
    last_data_time = None

    # Run loop for receiving the data
    while True:
        if ser.inWaiting() > 0:
            # Read data from serial port
            try:
                data = ser.read(size=2)
                data_count = data_count + 1
            except serial.SerialException as e:
                print(f"Error reading data: {str(e)}")
                break

            # Convert bytes to integer
            try:
                integer_data = int.from_bytes(data, byteorder='little', signed=True)
            except Exception as e:
                print(f"Error converting data: {str(e)}")
                break

            # Append to data
            audio_data.append(integer_data)

            # Update the last data time
            last_data_time = time.time()

        # If there is no data for 'read_timeout' seconds, stop reading
        elif last_data_time is not None and time.time() - last_data_time > read_timeout:
            print(f"Timeout occurred. Data received: {data_count * 2} bytes")
            break

    # Close the serial port
    ser.close()

    #######################################################################################################################
    # SAVE THE DATA INTO WAV FILE
    #######################################################################################################################

    # Apply the high-pass filter to your audio data
    # cutoff_frequency = 200  # for example, remove frequencies below 100 Hz
    # audio_data = highpass_filter(audio_data, cutoff_frequency, sample_rate)

    # Apply the low-pass filter to your audio data
    # cutoff_frequency_high = 3900  # for example, remove frequencies above 3000 Hz
    # audio_data = lowpass_filter(audio_data, cutoff_frequency_high, sample_rate)

    # Open a wave file and write data
    t = time.localtime()
    timestamp = time.strftime('%b-%d-%Y_%H%M%S', t)

    try:
        with wave.open(timestamp + '_audio.wav', 'w') as wav_file:
            wav_file.setnchannels(channels)
            wav_file.setsampwidth(2)  # number of bytes per sample, 16 bits = 2 bytes
            wav_file.setframerate(sample_rate)
            wav_file.writeframes(np.array(audio_data, dtype=np.int16).tobytes())

        print("File saved successfully.")
    except Exception as e:
        print(f"Error saving file: {str(e)}")
