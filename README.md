# Wallebee

# Sensor Device User Manual

## Table of Contents
- [Introduction](#introduction)
- [Purpose](#purpose)
- [Intended Audience and Pertinent Sections](#intended-audience-and-pertinent-sections)
- [Project Scope](#project-scope)
- [Document Conventions](#document-conventions)
- [References](#references)
- [Description](#description)
- [Product Perspective](#product-perspective)
- [Features](#features)
- [User Overview](#user-overview)
- [Operating Environment](#operating-environment)
- [Constraints: Implementation / Design](#constraints-implementation-design)
- [Documentation](#documentation)
- [Assumptions / Dependencies](#assumptions-dependencies)
- [System Features](#system-features)
  - [System Feature 1](#system-feature-1)
  - [System Feature 2](#system-feature-2)
  - [System Feature 3](#system-feature-3)
- [Requirements of External Interface](#requirements-of-external-interface)
  - [User Interfaces](#user-interfaces)
  - [Hardware Interfaces](#hardware-interfaces)
  - [Software Interfaces](#software-interfaces)
  - [Communication Interfaces](#communication-interfaces)
- [Additional Nonfunctional Requirements](#additional-nonfunctional-requirements)
  - [Performance](#performance)
  - [Safety](#safety)
  - [Security](#security)
  - [Software Quality](#software-quality)
- [Appendices](#appendices)
  - [Appendix A: Glossary of Terms](#appendix-a-glossary-of-terms)
  - [Appendix B: Analysis Documentation](#appendix-b-analysis-documentation)
  - [Appendix C: Issues](#appendix-c-issues)

---

## Introduction

## Purpose
The product aims to assist beekeepers in remotely monitoring hive conditions and providing early warnings about potential problems to address them promptly.

## Features

### Current Features:
- Detection of brood start and end.
- Queen bee piping.
- Unauthorized hive opening alerts.

### Planned Features:
- Detection of queenlessness.
- Detection of food shortages.
- Identification of robbing activity.
- Rapid nectar intake detection.

### Device Specification
- **Dimensions:** 43mm x 50mm x 19.7mm.
- **Battery:** 3.7V Li-Ion 3000 mAh, lasting 10 months.
- **Data Transmission:** LoRa network (3km - 10km radius).
- **Operating Environment:** -20°C to 40°C, IP44.

### Feature Detection Details

#### Brood Start and End Detection
Uses HDC2021 temperature sensor to detect temperature changes between 25°C - 35°C. More details in [sensor datasheet](https://www.ti.com/lit/ds/symlink/hdc2021.pdf).

#### Queen Bee Piping Detection
- Recognizes queen bee piping using the MP34DT06JTR microphone sensor ([datasheet](https://www.st.com/resource/en/datasheet/mp34dt06j.pdf)).
- Spectrogram-based detection with machine learning.

![Queen Bee Piping Spectrogram](media/queen_bee_piping.png)

# Project Documentation

## See BOM File

[Download the BOM File](./media/ibom.html)

To view the BOM, download the file and open it in any modern web browser.

## See BOM File

[See BOM File](https://gitlab.com/guntisl/wallebee/media/bomi.html){:target="_blank"}

## See BOM File

**[See BOM File](./media/ibom.html)**  
> **Tip:** If the file opens as raw code, right-click and select "Open Link in New Tab."



```mermaid



graph TD
    A[Queen Bee Piping] -->|Record Sound| B[Convert to Spectrogram]
    B --> C[Analyze with Machine Learning]
    C --> D[Alert Beekeeper]
```

#### Hive Opening Detection
Detects hive opening using a magnetic sensor when the magnet moves 3-5 cm away from the device.

```mermaid
graph TD
    A[Magnet in Place] -->|Magnet Moves| B[Sensor Triggered]
    B --> C[Alert Sent to User]
```

#### Planned Features Workflow

```mermaid
graph TD
    A[New Behavior Detected] --> B[Train System with Data]
    B --> C[Deploy Trained Model]
    C --> D[Alert Beekeeper]
```

## Appendices

### Appendix A: Glossary of Terms
- **Brood:** The developing bees in a hive.
- **Piping:** Sound made by a queen bee.
- **Robbing:** Theft of honey by other bee colonies.

### Appendix B: Analysis Documentation
- Sensor details: [HDC2021 Datasheet](https://www.ti.com/lit/ds/symlink/hdc2021.pdf).
- Microphone details: [MP34DT06JTR Datasheet](https://www.st.com/resource/en/datasheet/mp34dt06j.pdf).

### Appendix C: Issues
- Pending: Detection system for rapid nectar intake.
- TBD: Food shortage detection feature.


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/guntisl/wallebee.git
git branch -M main
git push -uf origin main
```

# System Flowchart

```mermaid
flowchart TB
    n74["Check Warning Button"] -- Pressed --> n73["Change Warning\nLED state"]
    n74 -- Not Pressed --> n75["Check Hive rate\nButton Press"]
    n75 -- Pressed --> n72["Set Hive rate\nLeds state"]
    nc["Power On!"] --> nb["Initializes peripherals and sensors"]
    nb --> ni["5 Seconds wait for tests"]
    ni -- Test Received --> n3["Checking\nwhich test\nis received"]
    n3 ---- LEDs["LEDs"] & AI["AI"] & Temperature["Temperature"] & Battery["Battery"] & LCD["LCD"] & Buttons["Buttons"]
    LEDs ----> nw["Done testing"]
    AI ----> nw
    Temperature ----> nw
    Battery ----> nw
    LCD ----> nw
    Buttons ----> nw
    n73 --> n76["Change Warning state"]
    n76 --> n75
    n75 -- Not Pressed --> n77["Check\nMotion\nstate"]
    n77 -- Motion detected --> n78["Change Motion State"]
    n77 -- No motion --> n81["Check\nMicroswitch\nState"]
    n72 --> n79["Set Hive rate state"]
    n81 -- Pressed --> n82["Change Microswitch state"]
    n79 --> n77
    n81 -- Not Pressed --> n84["Join to LORA\nNetwork"]
    n78 --> n81
    n82 --> n84
    n84 --> n85["Collect data\nfor LORA\nmessage"]
    n85 --> n86["Send Data\nmessage\nover LORA"]
    n86 --> n87["Check Received\nMessage from app"]
    n87 -- No RX MSG --> n89["Sensor Power OFF"]
    n87 -- RX Alarm --> n88["Change alarm state"]
    n87 -- RX User Led --> n90["Change User Led state"]
    n88 --> n89
    n90 --> n89
    n89 --> n91["Check for\nDay or Night"]
    n91 -- Night --> n92["Go to Sleep\nfor 4 Hours"]
    n91 -- Day --> n93["Go to sleep\nfor 10 minutes"]
    ni -- 5s pass or Tests are Done --> n74
    n92 --> n94["Wake UP"]
    n93 --> n94
    n94 --> n95["Sensor Power ON"]
    n95 --> n74
    nw -- Return to testloop --> ni
