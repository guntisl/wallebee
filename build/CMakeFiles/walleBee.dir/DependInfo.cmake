
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/home/guntis/dev/wallebee/startup_stm32u599xx.s" "/home/guntis/dev/wallebee/build/CMakeFiles/walleBee.dir/startup_stm32u599xx.s.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "DEBUG"
  "STM32U599xx"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../Core/Inc"
  "../cmake/stm32cubemx/../../Core/Inc"
  "../cmake/stm32cubemx/../../Drivers/STM32U5xx_HAL_Driver/Inc"
  "../cmake/stm32cubemx/../../Drivers/STM32U5xx_HAL_Driver/Inc/Legacy"
  "../cmake/stm32cubemx/../../Drivers/CMSIS/Device/ST/STM32U5xx/Include"
  "../cmake/stm32cubemx/../../Drivers/CMSIS/Include"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/guntis/dev/wallebee/Core/Src/brooding_monitor.c" "CMakeFiles/walleBee.dir/Core/Src/brooding_monitor.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/brooding_monitor.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/cat24m01.c" "CMakeFiles/walleBee.dir/Core/Src/cat24m01.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/cat24m01.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/ds18b20.c" "CMakeFiles/walleBee.dir/Core/Src/ds18b20.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/ds18b20.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/file_receiver.c" "CMakeFiles/walleBee.dir/Core/Src/file_receiver.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/file_receiver.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/flash.c" "CMakeFiles/walleBee.dir/Core/Src/flash.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/flash.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/fonts.c" "CMakeFiles/walleBee.dir/Core/Src/fonts.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/fonts.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/lora.c" "CMakeFiles/walleBee.dir/Core/Src/lora.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/lora.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/main.c" "CMakeFiles/walleBee.dir/Core/Src/main.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/main.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/mc3630.c" "CMakeFiles/walleBee.dir/Core/Src/mc3630.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/mc3630.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/power.c" "CMakeFiles/walleBee.dir/Core/Src/power.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/power.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/rtc_handler.c" "CMakeFiles/walleBee.dir/Core/Src/rtc_handler.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/rtc_handler.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/sht40.c" "CMakeFiles/walleBee.dir/Core/Src/sht40.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/sht40.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/ssd1306.c" "CMakeFiles/walleBee.dir/Core/Src/ssd1306.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/ssd1306.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/stm32u5xx_hal_msp.c" "CMakeFiles/walleBee.dir/Core/Src/stm32u5xx_hal_msp.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/stm32u5xx_hal_msp.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/stm32u5xx_it.c" "CMakeFiles/walleBee.dir/Core/Src/stm32u5xx_it.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/stm32u5xx_it.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/syscalls.c" "CMakeFiles/walleBee.dir/Core/Src/syscalls.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/syscalls.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/sysmem.c" "CMakeFiles/walleBee.dir/Core/Src/sysmem.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/sysmem.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/system_stm32u5xx.c" "CMakeFiles/walleBee.dir/Core/Src/system_stm32u5xx.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/system_stm32u5xx.c.obj.d"
  "/home/guntis/dev/wallebee/Core/Src/w25qxx.c" "CMakeFiles/walleBee.dir/Core/Src/w25qxx.c.obj" "gcc" "CMakeFiles/walleBee.dir/Core/Src/w25qxx.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_adc.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_adc.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_adc.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_adc_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_adc_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_adc_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_cortex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_cortex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_cortex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dac.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dac.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dac.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dac_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dac_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dac_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dma.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dma.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dma.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dma_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dma_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_dma_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_exti.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_exti.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_exti.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_flash.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_flash.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_flash.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_flash_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_flash_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_flash_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_gpio.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_gpio.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_gpio.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_gtzc.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_gtzc.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_gtzc.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_i2c.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_i2c.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_i2c.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_i2c_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_i2c_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_i2c_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_icache.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_icache.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_icache.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_pwr.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_pwr.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_pwr.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_pwr_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_pwr_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_pwr_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rcc.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rcc.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rcc.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rcc_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rcc_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rcc_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rtc.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rtc.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rtc.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rtc_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rtc_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_rtc_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_sai.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_sai.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_sai.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_sai_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_sai_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_sai_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_spi.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_spi.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_spi.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_spi_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_spi_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_spi_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_tim.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_tim.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_tim.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_tim_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_tim_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_tim_ex.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_uart.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_uart.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_uart.c.obj.d"
  "/home/guntis/dev/wallebee/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_uart_ex.c" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_uart_ex.c.obj" "gcc" "CMakeFiles/walleBee.dir/Drivers/STM32U5xx_HAL_Driver/Src/stm32u5xx_hal_uart_ex.c.obj.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
