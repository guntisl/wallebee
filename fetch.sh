#!/bin/bash

# Navigate to the git repository directory
cd "$(dirname "$0")"

# Fetch the most recent updates from the remote repository
git fetch origin

# Optionally, merge or rebase to update the working directory
git merge origin/main  # Replace 'main' with your branch name, if different
# Or use git rebase origin/main if you prefer rebase
