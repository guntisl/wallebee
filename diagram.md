# System Flowchart

```mermaid
flowchart TB
    n74["Check Warning Button"] -- Pressed --> n73["Change Warning\nLED state"]
    n74 -- Not Pressed --> n75["Check Hive rate\nButton Press"]
    n75 -- Pressed --> n72["Set Hive rate\nLeds state"]
    nc["Power On!"] --> nb["Initializes peripherals and sensors"]
    nb --> ni["5 Seconds wait for tests"]
    ni -- Test Received --> n3["Checking\nwhich test\nis received"]
    n3 ---- LEDs["LEDs"] & AI["AI"] & Temperature["Temperature"] & Battery["Battery"] & LCD["LCD"] & Buttons["Buttons"]
    LEDs ----> nw["Done testing"]
    AI ----> nw
    Temperature ----> nw
    Battery ----> nw
    LCD ----> nw
    Buttons ----> nw
    n73 --> n76["Change Warning state"]
    n76 --> n75
    n75 -- Not Pressed --> n77["Check\nMotion\nstate"]
    n77 -- Motion detected --> n78["Change Motion State"]
    n77 -- No motion --> n81["Check\nMicroswitch\nState"]
    n72 --> n79["Set Hive rate state"]
    n81 -- Pressed --> n82["Change Microswitch state"]
    n79 --> n77
    n81 -- Not Pressed --> n84["Join to LORA\nNetwork"]
    n78 --> n81
    n82 --> n84
    n84 --> n85["Collect data\nfor LORA\nmessage"]
    n85 --> n86["Send Data\nmessage\nover LORA"]
    n86 --> n87["Check Received\nMessage from app"]
    n87 -- No RX MSG --> n89["Sensor Power OFF"]
    n87 -- RX Alarm --> n88["Change alarm state"]
    n87 -- RX User Led --> n90["Change User Led state"]
    n88 --> n89
    n90 --> n89
    n89 --> n91["Check for\nDay or Night"]
    n91 -- Night --> n92["Go to Sleep\nfor 4 Hours"]
    n91 -- Day --> n93["Go to sleep\nfor 10 minutes"]
    ni -- 5s pass or Tests are Done --> n74
    n92 --> n94["Wake UP"]
    n93 --> n94
    n94 --> n95["Sensor Power ON"]
    n95 --> n74
    nw -- Return to testloop --> ni
