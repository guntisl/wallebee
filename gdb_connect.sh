#!/bin/bash

# Start OpenOCD and wait for GDB connection
openocd -f interface/stlink.cfg -c "transport select hla_swd" \
        -f target/stm32u5x.cfg \
        -c "init" \
        -c "reset halt" \
        -c "gdb port 3333" \
        -c "telnet_port 4444" \
        -c "tcl_port 6666"
