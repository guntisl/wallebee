#!/bin/bash
cd ./build/

make

cd ../



# Check for correct usage
if [ $# -ne 1 ]; then
    echo "Usage: $0 <firmware.bin|firmware.elf>"
    exit 1
fi

FIRMWARE=$1

# Start OpenOCD with the necessary configurations and commands
openocd -f interface/stlink.cfg -c "transport select hla_swd" \
        -f target/stm32u5x.cfg \
        -c "init" \
        -c "reset halt" \
        -c "program $FIRMWARE verify reset exit"
