#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <sys/poll.h>

#define SERIAL_PORT "/dev/ttyUSB0" // Change to your serial port
#define BAUD_RATE B115200

void configureSerialPort(int fd) {
    struct termios options;

    tcgetattr(fd, &options);
    cfsetispeed(&options, BAUD_RATE);
    cfsetospeed(&options, BAUD_RATE);

    options.c_cflag |= (CLOCAL | CREAD);
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CRTSCTS;

    options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    options.c_iflag &= ~(IXON | IXOFF | IXANY);
    options.c_iflag &= ~(ICRNL | INLCR);

    options.c_oflag &= ~OPOST;

    tcsetattr(fd, TCSANOW, &options);
}

void sendFile(int fd, const char *filePath) {
    FILE *file = fopen(filePath, "rb");
    if (!file) {
        perror("Failed to open file");
        exit(EXIT_FAILURE);
    }

    uint8_t buffer[1024];
    size_t bytesRead;

    while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
        write(fd, buffer, bytesRead);

        // Poll for data to ensure it's sent before reading more
        struct pollfd fds;
        fds.fd = fd;
        fds.events = POLLOUT;

        int ret = poll(&fds, 1, 1000); // 1 second timeout
        if (ret == -1) {
            perror("poll");
            fclose(file);
            exit(EXIT_FAILURE);
        }
    }

    fclose(file);
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s -f|-l <file_path>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int fd = open(SERIAL_PORT, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1) {
        perror("Unable to open serial port");
        exit(EXIT_FAILURE);
    }

    configureSerialPort(fd);

    char command[5] = {0};
    if (strcmp(argv[1], "-f") == 0) {
        strcpy(command, "FWUP");
    } else if (strcmp(argv[1], "-l") == 0) {
        strcpy(command, "LIBU");
    } else {
        fprintf(stderr, "Invalid option: %s\n", argv[1]);
        close(fd);
        exit(EXIT_FAILURE);
    }

    // Send command to initiate file reception
    write(fd, command, strlen(command));

    // Poll for data to ensure the command is sent
    struct pollfd fds;
    fds.fd = fd;
    fds.events = POLLOUT;

    int ret = poll(&fds, 1, 1000); // 1 second timeout
    if (ret == -1) {
        perror("poll");
        close(fd);
        exit(EXIT_FAILURE);
    }

    // Send the file
    sendFile(fd, argv[2]);

    close(fd);
    return 0;
}
