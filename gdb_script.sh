#!/bin/bash

# Script name: start_gdb_session.sh
# Usage: ./start_gdb_session.sh

# Define variables
SESSION="debug_session"
BUILD_DIR="build"
ELF_FILE="build/stm32_v1.elf"
OPENOCD_CMD="openocd -s /usr/local/share/openocd/scripts -f interface/stlink.cfg -f target/stm32u5x.cfg"

# Start a new tmux session
tmux new-session -d -s $SESSION

# Split the window into two panes
tmux split-window -h

# In the first pane, change to the build directory, run make, and check the result
tmux send-keys -t $SESSION:0.0 "cd $BUILD_DIR && make" C-m
# Check if make was successful
tmux send-keys -t $SESSION:0.0 "if [ \$? -eq 0 ]; then $OPENOCD_CMD; else echo 'Build failed. Not starting OpenOCD.'; fi" C-m

# In the second pane, start gdb-multiarch and run the GDB commands
tmux send-keys -t $SESSION:0.1 "gdb-multiarch $ELF_FILE" C-m
tmux send-keys -t $SESSION:0.1 "target extended-remote localhost:3333" C-m
tmux send-keys -t $SESSION:0.1 "layout next" C-m
tmux send-keys -t $SESSION:0.1 "break main" C-m

# Attach to the tmux session
tmux attach -t $SESSION
