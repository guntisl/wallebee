#!/bin/bash

# Start OpenOCD with the necessary configurations and commands
openocd -f interface/stlink.cfg -c "transport select hla_swd" \
        -f target/stm32u5x.cfg \
        -c "init" \
