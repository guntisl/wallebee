/*
 * cat24m01.c
 *
 *  Created on: 2023. gada 21. jūn.
 *      Author: oskars_selis
 */

#include <cat24m01.h>


HAL_StatusTypeDef EEPROM_Init(I2C_HandleTypeDef *hi2c)
{
	return HAL_I2C_IsDeviceReady(hi2c, EEPROM_I2C_ADDRESS_1, 1, I2C_TIMEOUT);
}

HAL_StatusTypeDef EEPROM_Write(I2C_HandleTypeDef *hi2c, uint16_t mem_addr, uint8_t* data, uint16_t size, uint8_t pageSelect)
{
    HAL_StatusTypeDef status = HAL_OK;
    uint8_t eeprom_address;

	if (pageSelect == 1)
	{
		eeprom_address = EEPROM_I2C_ADDRESS_2;
	}
	else
	{
		eeprom_address = EEPROM_I2C_ADDRESS_1;
	}

    while (size > EEPROM_PAGE_SIZE)
    {
        status = HAL_I2C_Mem_Write(hi2c, eeprom_address, mem_addr, I2C_MEMADD_SIZE_16BIT, data, EEPROM_PAGE_SIZE, I2C_TIMEOUT);
        if (status != HAL_OK)
        {
            return status;
        }

        // Update the memory address, data pointer and size
        mem_addr += EEPROM_PAGE_SIZE;
        data += EEPROM_PAGE_SIZE;
        size -= EEPROM_PAGE_SIZE;

        // Delay to allow EEPROM to complete the write operation
        HAL_Delay(3);
    }

    if (size > 0)  // If there are still data left, write it to EEPROM
    {
        status = HAL_I2C_Mem_Write(hi2c, eeprom_address, mem_addr, I2C_MEMADD_SIZE_16BIT, data, size, I2C_TIMEOUT);
        HAL_Delay(3);
    }

    return status;
}

HAL_StatusTypeDef EEPROM_Read(I2C_HandleTypeDef *hi2c, uint16_t mem_addr, uint8_t* data, uint16_t size, uint8_t pageSelect)
{
    HAL_StatusTypeDef status = HAL_OK;
    uint8_t eeprom_address;

	if (pageSelect == 1)
	{
		eeprom_address = EEPROM_I2C_ADDRESS_2;
	}
	else
	{
		eeprom_address = EEPROM_I2C_ADDRESS_1;
	}

    while (size > EEPROM_PAGE_SIZE)
    {
        status = HAL_I2C_Mem_Read(hi2c, eeprom_address, mem_addr, I2C_MEMADD_SIZE_16BIT, data, EEPROM_PAGE_SIZE, I2C_TIMEOUT);
        if (status != HAL_OK)
        {
            return status;
        }

        // Update the memory address, data pointer and size
        mem_addr += EEPROM_PAGE_SIZE;
        data += EEPROM_PAGE_SIZE;
        size -= EEPROM_PAGE_SIZE;

        HAL_Delay(3);
    }

    if (size > 0)  // If there are still data left, read it from EEPROM
    {
        status = HAL_I2C_Mem_Read(hi2c, eeprom_address, mem_addr, I2C_MEMADD_SIZE_16BIT, data, size, I2C_TIMEOUT);
        HAL_Delay(3);
    }

    return status;
}
