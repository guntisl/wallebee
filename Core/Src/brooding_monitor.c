// brooding_monitor.c

#include "brooding_monitor.h"
#include "rtc_handler.h"
#include "sht40.h"
extern I2C_HandleTypeDef hi2c2;
extern I2C_HandleTypeDef hi2c4;
extern I2C_HandleTypeDef hi2c5;

#define TEMP_THRESHOLD 35.0


BroodingState checkBroodingState(void) {
    float temp1 = SHT40_measure_temperature(&hi2c2);
    float temp2 = SHT40_measure_temperature(&hi2c4);
    float temp3 = SHT40_measure_temperature(&hi2c5);

    int aboveThresholdCount = 0;
    aboveThresholdCount += (temp1 > TEMP_THRESHOLD);
    aboveThresholdCount += (temp2 > TEMP_THRESHOLD);
    aboveThresholdCount += (temp3 > TEMP_THRESHOLD);

    if (aboveThresholdCount == 3) {
        return BROODING_STRONG;
    } else if (aboveThresholdCount == 2) {
        return BROODING_NORMAL;
    } else if (aboveThresholdCount == 1) {
        return BROODING_WEAK;
    } else {
        return BROODING_NONE;
    }
}

void monitorBroodingQuality(void) {
    static BroodingState lastState = BROODING_NONE;
    static uint32_t lastCheckedDay = 0;
    static int dayCounter = 0;
    static int strongBroodingDays = 0;

    BroodingState currentState = checkBroodingState();
    uint32_t currentDay = getCurrentDay();

    if (currentDay != lastCheckedDay) {
        if (currentState == BROODING_STRONG) {
            strongBroodingDays++;
        } else {
            strongBroodingDays = 0;
        }

        if (currentState != lastState) {
            dayCounter++;
            lastState = currentState;
        }

        lastCheckedDay = currentDay;

        // Evaluate brooding quality
        if (strongBroodingDays >= 7) {
            // Great brooding quality
        } else if (dayCounter >= 1 && dayCounter <= 2) {
            // Great brooding quality
        } else if (dayCounter >= 3 && dayCounter <= 5) {
            // Good brooding quality
        } else if (dayCounter >= 7 && currentState == BROODING_NORMAL) {
            // Bad brooding quality
        }
    }
}
