
#include "main.h"
#include "ds18b20.h"
extern TIM_HandleTypeDef htim6;
#define DS18B20_SKIP_ROM 0xCC
#define DS18B20_CONVERT_T 0x44
#define DS18B20_READ_SCRATCHPAD 0xBE

// Delay functions (in microseconds)
void delay_us(uint16_t us) {
//    __HAL_TIM_SET_COUNTER(&htim1, 0);  // htim1 needs to be a pre-configured timer
  //  while (__HAL_TIM_GET_COUNTER(&htim1) < us);
  //
     // Calculate the number of timer ticks for the desired delay in microseconds
    uint32_t ticks = us * 1.568; // TIM6 clock is approximately 1.568 MHz

    // Ensure the ticks fit within the timer's period
    if (ticks > htim6.Init.Period) {
        ticks = htim6.Init.Period;  // Limit the delay to the maximum period
    }

    // Reset the timer counter
    __HAL_TIM_SET_COUNTER(&htim6, 0);

    // Start the timer
    HAL_TIM_Base_Start(&htim6);

    // Wait until the counter reaches the number of ticks for the desired delay
    while (__HAL_TIM_GET_COUNTER(&htim6) < ticks);

    // Stop the timer
    HAL_TIM_Base_Stop(&htim6);
}

// 1-Wire Reset
uint8_t DS18B20_Reset(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) {
    uint8_t response = 0;
    HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);  // Pull the line low
    delay_us(480);                                       // 480us delay
    HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);    // Release the line
    delay_us(80);                                        // 80us delay
    response = HAL_GPIO_ReadPin(GPIOx, GPIO_Pin);        // Read the response
    delay_us(400);                                       // Wait for the line to return to idle
    return response;                                     // Return 0 if presence pulse detected
}

// Write a single bit to the DS18B20
void DS18B20_WriteBit(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint8_t bit) {
    if (bit) {
        HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
        delay_us(1);  // 1us write time
        HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
        delay_us(60);  // Recovery time
    } else {
        HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
        delay_us(60);  // 60us low time
        HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
    }
}

// Read a single bit from the DS18B20
uint8_t DS18B20_ReadBit(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) {
    uint8_t value = 0;
    HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
    delay_us(1);  // 1us delay to start reading
    HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);  // Release the line
    delay_us(15);                                     // Wait for the sensor to drive the line
    value = HAL_GPIO_ReadPin(GPIOx, GPIO_Pin);        // Read the value
    delay_us(45);                                     // Recovery time
    return value;
}

// Write a byte to the DS18B20
void DS18B20_WriteByte(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint8_t byte) {
    for (uint8_t i = 0; i < 8; i++) {
        DS18B20_WriteBit(GPIOx, GPIO_Pin, byte & 0x01);
        byte >>= 1;
    }
}

// Read a byte from the DS18B20
uint8_t DS18B20_ReadByte(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) {
    uint8_t value = 0;
    for (uint8_t i = 0; i < 8; i++) {
        value |= (DS18B20_ReadBit(GPIOx, GPIO_Pin) << i);
    }
    return value;
}

// Initialize GPIO pins for DS18B20 sensors
void DS18B20_Init(void) {
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    // Initialize GPIO pins for each sensor
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();

    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;

    // PB2, PB8, PB9, PB12
    GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_12;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    // PD9, PD7, PD4, PD3
    GPIO_InitStruct.Pin = GPIO_PIN_9 | GPIO_PIN_7 | GPIO_PIN_4 | GPIO_PIN_3;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
}

// Read temperature from the specified sensor
float DS18B20_ReadTemp(DS18B20_Sensor_t sensor) {
    GPIO_TypeDef *GPIOx;
    uint16_t GPIO_Pin;

    // Map sensor identifier to corresponding GPIO port and pin
    switch (sensor) {
        case SENSOR_1:
            GPIOx = GPIOB;
            GPIO_Pin = GPIO_PIN_2;
            break;
        case SENSOR_2:
            GPIOx = GPIOB;
            GPIO_Pin = GPIO_PIN_8;
            break;
        case SENSOR_3:
            GPIOx = GPIOB;
            GPIO_Pin = GPIO_PIN_9;
            break;
        case SENSOR_4:
            GPIOx = GPIOB;
            GPIO_Pin = GPIO_PIN_12;
            break;
        case SENSOR_5:
            GPIOx = GPIOD;
            GPIO_Pin = GPIO_PIN_9;
            break;
        case SENSOR_6:
            GPIOx = GPIOD;
            GPIO_Pin = GPIO_PIN_7;
            break;
        case SENSOR_7:
            GPIOx = GPIOD;
            GPIO_Pin = GPIO_PIN_4;
            break;
        case SENSOR_8:
            GPIOx = GPIOD;
            GPIO_Pin = GPIO_PIN_3;
            break;
        default:
            return -127.0;  // Error, invalid sensor ID
    }

    uint8_t temp_lsb, temp_msb;
    int16_t temp_raw;
    float temperature;

    if (DS18B20_Reset(GPIOx, GPIO_Pin) == 0) {
        DS18B20_WriteByte(GPIOx, GPIO_Pin, DS18B20_SKIP_ROM);    // Skip ROM command
        DS18B20_WriteByte(GPIOx, GPIO_Pin, DS18B20_CONVERT_T);   // Start temperature conversion
        HAL_Delay(750);                                          // Wait for conversion (~750ms for 12-bit resolution)

        DS18B20_Reset(GPIOx, GPIO_Pin);                          // Reset again
        DS18B20_WriteByte(GPIOx, GPIO_Pin, DS18B20_SKIP_ROM);    // Skip ROM command
        DS18B20_WriteByte(GPIOx, GPIO_Pin, DS18B20_READ_SCRATCHPAD);  // Read scratchpad command

        temp_lsb = DS18B20_ReadByte(GPIOx, GPIO_Pin);  // Read LSB
        temp_msb = DS18B20_ReadByte(GPIOx, GPIO_Pin);  // Read MSB
        temp_raw = (temp_msb << 8) | temp_lsb;         // Combine LSB and MSB

        temperature = temp_raw / 16.0;  // Convert to Celsius
    } else {
        temperature = -127.0;  // Error reading sensor
    }

    return temperature;
}

