// power.c

#include "power.h"

extern RTC_HandleTypeDef hrtc; // Ensure this is declared properly

void enterStopMode(void) {
    HAL_SuspendTick();
    HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_STOPENTRY_WFI);
    // Optionally configure the system clock after wake-up
    // systemClockConfigForLowPower();
    HAL_ResumeTick();
}

void enterStandbyMode(void) {
    HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1); // Ensure this pin is connected correctly
    HAL_PWR_EnterSTANDBYMode();
}

void sleepWithWakeupSources(void) {
    RTC_TimeTypeDef time = {0};
    HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);

    if (time.Hours >= 6 && time.Hours < 22) {
        enterStopMode(); // Enter Stop mode during day time
    } else {
        enterStandbyMode(); // Enter Standby mode during night time
    }
}
