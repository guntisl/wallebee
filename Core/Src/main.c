/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "sht40.h"
#include "w25qxx.h"
//#include "piping24.h"
#include "stm32u5xx_ll_dac.h"
#include "stm32u599xx.h"
#include "sample.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ssd1306.h"
#include "fonts.h"
//#include "test.h"
#include "cat24m01.h"
#include "mc3630.h"
#include <stdarg.h>
#include "file_receiver.h"
#include "power.h"
#include "flash.h"
#include "rtc_handler.h"
#include "i2s_microphone.h"
#include "ds18b20.h"
#include "lora.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define SIZE_OF_BUFFER 3
#define	SIZE_OF_MSG 5

#define	AUDIO_DATA_FREE 1
#define TOTAL_BUFFERS 		255
#define BUFFER_SIZE 		128
#define AUDIO_BUFFER_SIZE 	4*BUFFER_SIZE
#define LED_IND_STATE_ADDR  ((uint32_t)0x0803F800)
#define FLASH_PAGE_NUM    	((uint32_t)LED_IND_STATE_ADDR - FLASH_BASE) / FLASH_PAGE_SIZE
#define RAW_BUFFER_SIZE		5000

#define CMD_BUFFER_SIZE 4


#define BROODING_TEMP 35
#define		LORA_DATE_SIZE 10
#define		LORA_TIME_SIZE 8


/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
int16_t readData[BUFFER_SIZE];
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc4;

DAC_HandleTypeDef hdac1;

I2C_HandleTypeDef hi2c2;
I2C_HandleTypeDef hi2c4;
I2C_HandleTypeDef hi2c5;

RTC_HandleTypeDef hrtc;

SAI_HandleTypeDef hsai_BlockB1;
DMA_NodeTypeDef Node_GPDMA1_Channel0;
DMA_QListTypeDef List_GPDMA1_Channel0;
DMA_HandleTypeDef handle_GPDMA1_Channel0;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim15;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

MC36XX_interrupt_event_t evt_mc36xx = { 0 };

uint8_t aShowTime[16] = "hh:ms:ss";
uint8_t aShowDate[16] = "dd-mm-yyyy";
//uint8_t loraTime[16] = "hh:ms:ss";
//uint8_t loraDate[16] = "dd-mm-yyyy";
__IO uint32_t RTCStatus = 0;

extern uint8_t warning_state = 0;

extern uint32_t j;
uint8_t cmdBuffer[CMD_BUFFER_SIZE + 1]; // Buffer to hold the received command (+1 for null-termination)

//uint16_t audio_rx[AUDIO_BUFFER_SIZE];
uint8_t uartBuffer[AUDIO_BUFFER_SIZE];


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_GPDMA1_Init(void);
static void MX_ADC4_Init(void);
static void MX_DAC1_Init(void);
static void MX_FLASH_Init(void);
static void MX_I2C2_Init(void);
static void MX_I2C4_Init(void);
static void MX_I2C5_Init(void);
static void MX_RTC_Init(void);
static void MX_SAI1_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM15_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART3_UART_Init(void);
/* USER CODE BEGIN PFP */

void audio_delay(void);
void state_message(uint8_t timing, uint8_t number, uint8_t period,
		uint8_t level, uint8_t warning);
static void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate);
void _send_audio_data(void);
void addToDisplay(uint8_t row, char screenText[]);
uint8_t broodingState(float temp1, float temp2, float temp3);
int8_t broodingQuality(uint8_t previusBroodingState, uint8_t currentBroodingState);
void processAudioData(int32_t *audioData);
//void loraJoin(void);
uint16_t daysCounter(uint8_t previusDay, uint8_t currentDay);
void addToDisplayBattery(char screenText[]);
void batteryLevel(void);
void beeFrame(int frameState);
void processBroodInfo(void);
void listenE5Lora(void);
void processAudio(void);
void setServo(uint8_t servo, int ccr);
void dacPlay( unsigned char playBuffer[], int sizeOfPlay);
void getLoraTime(void);
void sendLora(msg MESSAGE1);
void setLed(uint8_t led, uint8_t state);
float readBatteryLevel(void);

void processCommand(const char *command);
void receiveAndProcess(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

static int32_t audio_rx[AUDIO_BUFFER_SIZE];
//static uint16_t audio_tx[AUDIO_BUFFER_SIZE];
uint8_t audio_ready_flag;

#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif

PUTCHAR_PROTOTYPE {
	HAL_UART_Transmit(&huart1, (uint8_t*) &ch, 1, HAL_MAX_DELAY);
	return ch;
}

void* sbrk(size_t incr) {
	extern uint8_t __heap_base__;
	extern uint8_t __heap_end__;
	static uint8_t *p = &__heap_base__;
	static uint8_t *newp;
	newp = p + incr;
	if (newp > &__heap_end__) {
		errno = ENOMEM;
		return (void*) -1;
	}
	return p = newp;
}
volatile uint8_t halfBuffer = 0;
volatile uint8_t fullBuffer = 0;

uint16_t halfBufferCount = 0;
uint16_t amountOfDays = 0;
uint8_t currentBroodingState;
uint8_t readDay;
uint32_t i = 0;
//extern uint8_t halfBuffer;
//extern uint8_t fullBuffer;
volatile uint8_t audioDataSaved = 0;

volatile uint8_t audioDataRecorded = 0;
volatile uint8_t audioDataRecorded_byte = 0;

// Define audio buffer where to store audio data
int32_t audioBuffer[AUDIO_BUFFER_SIZE];
// uint8_t leftAudioBuffer[BUFFER_SIZE];
//  uint8_t rawAudioDataint[RAW_BUFFER_SIZE];
float rawAudioDatafloat[RAW_BUFFER_SIZE];

char E5Msg[20];
// Define audio buffer where to store audio data
//int32_t audioBuffer[AUDIO_BUFFER_SIZE];
// uint8_t leftAudioBuffer[BUFFER_SIZE];
//  uint8_t rawAudioDataint[RAW_BUFFER_SIZE];
//float rawAudioDatafloat[RAW_BUFFER_SIZE];

enum servoMotor {
	motor_1,
	motor_2,
	motor_3,
	motor_4,
	motor_5,
	motor_6,
	motor_7,
	motor_8,
	motor_9,
	motor_10
};

enum ccrPulses {
	fullyClose = 30,
	fullyOpen = 70,
	upwardsLeft = 90,
	upwardsRight = 70,
	downwardsLeft = 100,
	downwardsRight = 120
};

enum beeFrameState {
	open, close, upwadrs, downwards
};
enum beeFrameState frameState;
enum broodState {
	noBrood = 5, littleBrood = 6, lotOfBrood = 7
};
//enum broodingstate brood;
enum timing11 {
	Today, Before
};
enum period11 {
	Day, Days, Week, Weeks, Month, Months
};
//  enum level{Weak, Normal, Good, Strong, Excelent};
enum number11 {
	Zero, One, Two, Three, Four, Five, Six, Seven
};
enum warning11 {
	Warning_off, Warning_on
};
enum level11 {
	Weak, Normal, Good, Strong, Excelent
};

RTC_TimeTypeDef sTime1;
RTC_DateTypeDef sDate1;

volatile uint32_t previousMillis = 0;
volatile uint32_t currentMillis = 0;
extern volatile uint8_t longPressOccured;
volatile uint8_t motionState = 0;

extern msg message;
uint8_t buffer[200] = { 1, 2, 3, 4, 5, 6, 7, 89, 0, 1, 2, 3, 4, 5, 6, 7, 8, 67,
		77, 20, 0 };
uint8_t w_buf[] = "istarik.ru";
uint8_t bytes[2];
uint8_t buffer2[64] = { 3, 4, 2 };
uint8_t read2_buffer2[64] = { 0 };
uint8_t buf22[256] = { 0, };
float val = 1.2;
uint32_t var;
char buff[50];
enum timing11 timing1 = Today;
enum period11 period1 = Day;
enum level11 level1 = Strong;
enum number11 number1 = One;
enum warning11 warning1 = Warning_off;

uint8_t circularBuffer[SIZE_OF_BUFFER][SIZE_OF_MSG] = { 0 };
uint16_t circularBufferNumber[SIZE_OF_BUFFER] = { 0 };
uint16_t circularBufferPeriod[SIZE_OF_BUFFER] = { 0 };
uint16_t circularBufferLevel[SIZE_OF_BUFFER] = { 0 };
uint16_t circularBufferWarning[SIZE_OF_BUFFER] = { 0 };

// Define audio buffer where to store audio data
int32_t audioBuffer[AUDIO_BUFFER_SIZE];
int16_t readBuffer[BUFFER_SIZE];
int16_t leftAudioBuffer[BUFFER_SIZE];
int16_t rawAudioDataint[RAW_BUFFER_SIZE];
float rawAudioDatafloat[RAW_BUFFER_SIZE];

// uint16_t circularBuffer[SIZE_OF_BUFFER];
uint8_t readIndex = 0;	// Index of the read pointer
uint8_t writeIndex = 0;	// Index of the write pointer
uint8_t bufferLength = 0;	// Number of values in circular buffer






int get_feature_data(size_t offset, size_t length, float *out_ptr) {
	memcpy(out_ptr, rawAudioDatafloat + offset, length * sizeof(float));
	return 0;
}

void MC3630_Sniff() {
	MC3630stop(&hi2c2);
	MC3630setSniffThreshold(&hi2c2, MC36XX_AXIS_X, 3);
	MC3630setSniffThreshold(&hi2c2, MC36XX_AXIS_Y, 3);
	MC3630setSniffThreshold(&hi2c2, MC36XX_AXIS_Z, 3);
	MC3630setSniffDetectCount(&hi2c2, MC36XX_AXIS_X, 2);
	MC3630setSniffDetectCount(&hi2c2, MC36XX_AXIS_Y, 2);
	MC3630setSniffDetectCount(&hi2c2, MC36XX_AXIS_Z, 2);
	MC3630setSniffAndOrN(&hi2c2, MC36XX_ANDORN_OR);
	MC3630setSniffDeltaMode(&hi2c2, MC36XX_DELTA_MODE_C2P);
	MC3630setINTCtrl(&hi2c2, 0, 0, 0, 0, 1);
	MC3630sniff(&hi2c2);
}

void MC3630_Init() {
	if (MC3630start(&hi2c2) != 0) {
		return;
	}

	MC3630_Sniff();
}



void listenE5Lora(void) {

	if (HAL_UART_Receive(&huart1, buff, 4, 1000) == HAL_OK) {

		if (strcmp(buff, "cmd1") == 0)

		{
			//beeFrame(close);
			//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET);
		//	printf("Sesrvo 5 test\r\n");
		//	setServo(motor_5, fullyClose);
		//	setServo(motor_5, j = j+10);
		//	printf("i = %d\r\n", j);

		} else if (strcmp(buff, "cmd2") == 0)

		{
			//beeFrame(open);
			setServo(motor_5, fullyOpen);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET);
			HAL_UART_Transmit(&huart1, aShowDate, sizeof(aShowDate),
			HAL_MAX_DELAY);
			printf("\r\n");

		}
		HAL_UART_Transmit(&huart1, buff, 10, 20);
		printf("\r\n");
		memset(buff, 0, sizeof(buff));
	}

}

void dacPlay(unsigned char  playBuffer[], int sizeOfPlay) {
	uint8_t s = 0;

	while (i <= sizeOfPlay) {

		//	LL_DAC_ConvertData8RightAligned(DAC, LL_DAC_CHANNEL_1, ((100*divaam[i++])/100));
		HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_1, DAC_ALIGN_8B_R, playBuffer[s++]);
		HAL_DAC_Start(&hdac1, DAC_CHANNEL_1);

		audio_delay();
		s++;
	}
	s = 0;
	HAL_DAC_Stop(&hdac1, DAC_CHANNEL_1);

}


void Audio_Init() {
	//audio_ready_flag = 0;

	if (HAL_SAI_Receive_DMA(&hsai_BlockB1, (uint16_t*) audio_rx,
	AUDIO_BUFFER_SIZE) != HAL_OK) {
		Error_Handler();
	}
	///	if(HAL_SAI_Transmit_DMA(&hsai_BlockB1, (uint8_t *)audio_tx, AUDIO_BUFFER_SIZE) != HAL_OK){
	//		Error_Handler();
	//	}
}

void HAL_SAI_RxHalfCpltCallback(SAI_HandleTypeDef *hi2s) {
	halfBuffer = 1;
	//printf("AUDIO_DATA_READY_HALF\r\n");
}

void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hi2s) {
	fullBuffer = 1;
	//printf("AUDIO_DATA_READY_FULL\r\n");
}

void processAudioData(int32_t *audioData)
// This function takes audio data and size and gets left channel data for further processing.
// The data is saved in buffer and then in EEPROM memory
{

	// This function takes audio data and size and gets left channel data for further processing.
	// The data is saved in buffer and then in EEPROM memory

	// This function takes audio data and size and gets left channel data for further processing.
	// The data is saved in buffer and then in EEPROM memory

	int16_t leftSample;

	for (uint16_t i = 0; i < BUFFER_SIZE; i++)
	{
		// get left channel data from I2S buffer
		leftSample = (int16_t)audioData[i*2];

		// Reduce volume by -6 dB (shift >> 1), -12 dB (shift >> 2)
		leftSample = leftSample >> 2;

		// Store the left audio data in buffer
		leftAudioBuffer[i] = leftSample & 0xFFFF;
	}


	uint8_t pageSelect = 0;

	if (halfBufferCount > 255)
	{
		pageSelect = 1;
	}

	halfBufferCount++;
	int16_t addrToWrite = halfBufferCount * sizeof(int16_t) * BUFFER_SIZE;
	if (EEPROM_Write(&hi2c2, addrToWrite, (uint8_t*)leftAudioBuffer, sizeof(leftAudioBuffer), pageSelect) != HAL_OK)
	{
		return;
	}

	// Increase the number of processed buffers
}




void _send_audio_data()
// This function reads the data from EEPROM and sends through UART
{
for(uint16_t i = 0; i < (halfBufferCount); i++)
	{
		// Read the data from the EEPROM

		uint8_t pageSelect = 0;

		if (i > 255)
		{
			pageSelect = 1;
		}

		int16_t addrToRead = i * sizeof(int16_t) * BUFFER_SIZE;

		if (EEPROM_Read(&hi2c2, addrToRead, (uint8_t*)readData, sizeof(readData), pageSelect) != HAL_OK)
		{
//			sendUartTx1Msg("ERROR: (EEPROM TEST) Could not read the data from the memory\r\n");
		}

		// UART transmission
		uint8_t bytes[2];
		for(uint16_t i = 0; i < BUFFER_SIZE; i++)
		{
			bytes[0] = (readData[i] >> 8) & 0xFF;
			bytes[1] = (readData[i]) & 0xFF;
			HAL_UART_Transmit(&huart1, bytes, sizeof(bytes), HAL_MAX_DELAY);
		}
	}

}






void audio_delay(void) {
	__HAL_TIM_SET_COUNTER(&htim6, 0);  // set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(&htim6) < 5000)
		;  // wait for the counter to reach the us input in the parameter
}

void SHT40_Init() {
	if (SHT40Init(&hi2c2) != HAL_OK) {

		printf("TEMP1 Sensors NOK\r\n");
	}

	return;
}

uint16_t daysCounter(uint8_t previusDay, uint8_t currentDay) {

	if (previusDay != currentDay) {
		amountOfDays++;
		MESSAGE.broodingState = broodingState(SHT40_measure_temperature(&hi2c2),
				SHT40_measure_temperature(&hi2c4),
				SHT40_measure_temperature(&hi2c5));

		MESSAGE.broodingQuality = broodingQuality(MESSAGE.broodingState,
				currentBroodingState);

	} else {

	}
	return amountOfDays;

}

int8_t broodingQuality(uint8_t previusBroodingState,uint8_t currentBroodingState) {
	int8_t brQualityState;
	if (currentBroodingState - previusBroodingState <= 0) {
		amountOfDays++;
		//amountOfBroodingDays++;

	} else if (currentBroodingState - previusBroodingState == 1) {

		switch (amountOfDays) {
		case 0:
			brQualityState = 0;
			addToDisplay(2, "PERO STRAUJI");
			printf("Pero strauji %d dienas\r\n", MESSAGE.amountOfDays);
			break;
		case 1:
			brQualityState = 1;
			addToDisplay(2, "PERO VIDEJI");
			printf("Pero videji %d dienas\r\n", MESSAGE.amountOfDays);
			break;
		case 2:
			brQualityState = 2;
			addToDisplay(2, "PERO OK");
			printf("Pero normali %d dienas\r\n", MESSAGE.amountOfDays);
			break;

		case 3:
			brQualityState = 3;
			addToDisplay(2, "PERO LENI");
			printf("Pero leni %d dienas\r\n", MESSAGE.amountOfDays);
			break;
		default:
			brQualityState = 4;
		}

		amountOfDays = 0;

	} else if (currentBroodingState - previusBroodingState >= 2) {
		switch (amountOfDays) {
		case 0:
			brQualityState = 0;
			addToDisplay(2, "PERO STRAUJI");
			printf("Pero strauji %d dienas\r\n", MESSAGE.amountOfDays);
			break;
		case 1:
			brQualityState = 1;
			addToDisplay(2, "PERO VIDEJI");
			printf("Pero videji %d dienas\r\n", MESSAGE.amountOfDays);
			break;
		case 2:
			brQualityState = 2;
			addToDisplay(2, "PERO OK");
			printf("Pero OK %d dienas\r\n", MESSAGE.amountOfDays);
			break;

		case 3:
			brQualityState = 3;
			addToDisplay(2, "PERO LENI");
			printf("Pero leni %d dienas\r\n", MESSAGE.amountOfDays);
			break;
		default:
			brQualityState = 2;
			addToDisplay(2, "PERO OK");
			printf("Pero OK %d dienas\r\n", MESSAGE.amountOfDays);
		}
		amountOfDays = 0;

	}
	return brQualityState;
}



void SendAudioOverUART(void)
{
    for (uint32_t i = 0; i < AUDIO_BUFFER_SIZE; i++)
    {
        uint8_t bytes[2];
        bytes[0] = (leftAudioBuffer[i] >> 8) & 0xFF;  // High byte
        bytes[1] = leftAudioBuffer[i] & 0xFF;         // Low byte

        // Transmit the two bytes over UART
        HAL_UART_Transmit(&huart1, bytes, sizeof(bytes), HAL_MAX_DELAY);
        
        // Adjust delay to match your desired sample rate
        HAL_Delay(1); // Adjust this delay to match your desired sample rate
    }
}



uint8_t broodingState(float temp1, float temp2, float temp3) {

	uint8_t brState;
	float brooding_temp = BROODING_TEMP;
	if ((temp1 < brooding_temp) && (temp2 < brooding_temp)
			&& (temp3 < brooding_temp)) {

		brState = 5;

	} else if ((temp1 > brooding_temp) && (temp2 > brooding_temp)
			&& (temp3 < brooding_temp)) {
		brState = 6;

	} else if ((temp1 < brooding_temp) && (temp2 > brooding_temp)
			&& (temp3 > brooding_temp)) {
		brState = 6;

	} else if ((temp1 > brooding_temp) && (temp2 > brooding_temp)
			&& (temp3 > brooding_temp)) {
		brState = 7;

	}
	return brState;
}

void addToDisplay(uint8_t row, char screenText[]) {
	//uint8_t buf[] = "istarik.ru";
	//SSD1306_Init(); // initialize the display
	//SSD1306_Clear();
	//HAL_Delay(5000);
	//SSD1306_UpdateScreen();
	if (row == 1) {

		SSD1306_GotoXY(0, 0);
		SSD1306_Puts(screenText, &Font_11x18, 1);
		SSD1306_UpdateScreen();
		return;
	} else if (row == 2) {

		SSD1306_GotoXY(0, 22);
		SSD1306_Puts(screenText, &Font_11x18, 1);
		SSD1306_UpdateScreen();
		return;
	} else if (row == 3) {

		SSD1306_GotoXY(0, 44);
		SSD1306_Puts(screenText, &Font_11x18, 1);
		SSD1306_UpdateScreen();
		return;
	}
}

void addToDisplayBattery(char screenText[]) {
	SSD1306_GotoXY(96, 0);
	SSD1306_Puts(screenText, &Font_11x18, 1);
	SSD1306_GotoXY(116, 0);
	SSD1306_Puts("%", &Font_11x18, 1);
	SSD1306_UpdateScreen();
	return;

}

void setServo(uint8_t servo, int ccr) {

	switch (servo) {
	case motor_1:

		TIM3->CCR2 = ccr;
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_2);
		break;

	case motor_2:

		TIM15->CCR2 = ccr;
		HAL_TIM_PWM_Start(&htim15, TIM_CHANNEL_2);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim15, TIM_CHANNEL_2);
		break;
	case motor_3:

		TIM1->CCR4 = ccr;
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_4);
		break;
	case motor_4:

		TIM5->CCR4 = ccr;
		HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_4);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_4);
		break;
	case motor_5:

		TIM3->CCR3 = ccr;
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
		HAL_Delay(30);
		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_3);
		break;
	case motor_6:

		TIM3->CCR3 = ccr;
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
		break;
	case motor_7:

		TIM4->CCR2 = ccr;
		HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_2);
		break;
	case motor_8:

		TIM5->CCR3 = ccr;
		HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_3);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_3);
		break;
	case motor_9:

		TIM5->CCR1 = ccr;
		HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_1);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_1);
		break;
	case motor_10:

		TIM4->CCR4 = ccr;
		HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);
		HAL_Delay(3000);
		HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_4);
		break;

	default:

		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_2);
		HAL_TIM_PWM_Stop(&htim15, TIM_CHANNEL_2);
		HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_4);
		HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_4);
		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_3);
		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
		HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_2);
		HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_3);
		HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_1);
		HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_4);
	}
return;
}

void beeFrame(int frameState) {

	uint8_t motor[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	switch (frameState) {
	case open:
		for (i = 0; i > 10; i++) {
			setServo(motor[i], fullyOpen);
		}
		break;
	case close:
		for (i = 0; i > 10; i++) {
			setServo(motor[i], fullyClose);
		}
		break;
	case upwadrs:
		for (i = 0; i > 10; i = i + 2) {
			setServo(motor[i], 400);
		}
		for (i = 1; i > 9; i = i + 2) {
			setServo(motor[i], upwardsRight);
		}
		break;

	case downwards:
		for (i = 0; i > 10; i = i + 2) {
			setServo(motor[i], upwardsRight);
		}
		for (i = 1; i > 9; i = i + 2) {
			setServo(motor[i], downwardsLeft);
		}
		break;
	default:
		for (i = 0; i > 10; i++) {
			setServo(motor[i], fullyClose);
		}
	}
}




void sendLora(msg MESSAGE1) {

	E5ModuleMsg("AT+PMSG=%s%f%f%f%f%d%d%d%d%d%d%d%d%d%d%d%d\r\n", MESSAGE1.Vbatt,
			MESSAGE1.temp1, MESSAGE1.temp2, MESSAGE1.temp2, MESSAGE1.humidity,
			MESSAGE1.broodingState, MESSAGE1.broodingQuality,
			MESSAGE1.incomingState, MESSAGE1.swarmingState,
			MESSAGE1.hungerState, MESSAGE1.queenlessState, MESSAGE1.motionState,
			MESSAGE1.frameState, MESSAGE1.frameScript, MESSAGE1.alarmState,
			MESSAGE1.userLedState, MESSAGE1.amountOfDays);

}
void processCommand(const char *command) {
msg message;
char msgString[50];
	extern UART_HandleTypeDef huart1;
	extern UART_HandleTypeDef huart3;
	if (strncmp(command, "cmd1", 4) == 0) {
		// Send predefined string over UART3
		E5ModuleMsg("AT");
		//loraJoin();

	} else if (strncmp(command, "cmd2", 4) == 0) {
		// Toggle LED on GPIOC pin 4
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_5);
		//beeFrame(close);
		TIM3->CCR3 = fullyOpen;
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
		HAL_Delay(5);
		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_3);
		printf("Sesrvo 5 test done\r\n");

	} else if (strncmp(command, "cmd3", 4) == 0) {
		// Read battery level and process it
		//batteryLevel();
		TIM3->CCR3 = fullyClose;
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
//		HAL_Delay(30);
//		HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_3);
		printf("Sesrvo 5 test done\r\n");


	} else if (strncmp(command, "cmd4", 4) == 0) {
	
		setPayloadAndSend(&message);

    } else if (strncmp(command, "cmd5", 4) == 0) {
        // Start firmware upgrade process
      //  receiveFile("firmware");
	//  E5ModuleMsg("AT+ID=DevEui,\""DEV_EUI"\"");
//
//Process_AudioData(); 

    } else if (strncmp(command, "cmd6", 4) == 0) {
        // Start library update process
		E5ModuleMsg("AT+JOIN");
       // receiveFile("library");

    } else if (strncmp(command, "cmd7", 4) == 0) {
		// Read battery level and process it
		//batteryLevel();
		E5ModuleMsg("AT+KEY=APPKEY,\"" APPS_S_KEY "\"");

	} else if (strncmp(command, "cmd8", 4) == 0) {
        // Read temperature sensor value and print it over UART1
        float temperature = 22.34;
        char tempStr[50];
        snprintf(tempStr, sizeof(tempStr), "Temperature: %f °C\r\n", temperature);
        //HAL_UART_Transmit(&huart1, (uint8_t*)tempStr, strlen(tempStr), HAL_MAX_DELAY);

		HAL_UART_Transmit(&huart1, (uint8_t*)tempStr, strlen(tempStr), HAL_MAX_DELAY);
//		printf("Start float process");
    } else if (strncmp(command, "cmd9", 4) == 0) {
		
        // Start firmware upgrade process
      //  receiveFile("firmware");
	  printf("Start float process");
	  char temp1char[20] = {0}; // Buffer for formatt snprintf(MSG5, sizeof(MSG5), "AT+MSG=9%u%u%u%ued string	sprintf(adcMsg, "%lu\r\n", varPercents);
	char testBuffer[20] = {0}; // Buffer for formatt snprintf(MSG5, sizeof(MSG5), "AT+MSG=9%u%u%u%ued string	sprintf(adcMsg, "%lu\r\n", varPercents);
	snprintf(temp1char, sizeof(temp1char), "temp1=%.2f",  SHT40_measure_temperature(&hi2c2));
	printf("Temp1  = %s", temp1char);
//	MESSAGE.temp1 = SHT40_measure_temperature(&hi2c2);
//	MESSAGE.temp2 = SHT40_measure_temperature(&hi2c2);
//	MESSAGE.temp3 = SHT40_measure_temperature(&hi2c2);
      MESSAGE.broodingState = 20;
      MESSAGE.alarmState = 10;
	  MESSAGE.broodingQuality = 56;
	  MESSAGE.swarmingState = 35;
	  testBuffer[0] = MESSAGE.broodingState;
	testBuffer[1] =  MESSAGE.alarmState;
	testBuffer[2] =  MESSAGE.broodingQuality;
	testBuffer[3] =  MESSAGE.swarmingState;
	  char MSG5[50] = {0};
	char msgEnd = '"';

	  snprintf(MSG5, sizeof(MSG5), "AT+MSG=9%u%u%u%u",
             MESSAGE.broodingState, 
			  MESSAGE.alarmState, MESSAGE.broodingQuality, MESSAGE.swarmingState);
        HAL_UART_Transmit(&huart1, (uint8_t*)MSG5, strlen(MSG5), HAL_MAX_DELAY);
		HAL_UART_Transmit(&huart3, (uint8_t*)MSG5, strlen(MSG5), HAL_MAX_DELAY);
//	printf("AT+PMSG=%c",&MSG5[0]);

    } else if (strncmp(command, "cmd0", 4) == 0) {
        // Start library update process
	//	E5ModuleMsg("AT+CMSGHEX=2525252525252525252525252525");
//		E5ModuleMsg("AT+PMSG=\""2525252525252525252525252525"\"");
       // receiveFile("library");

    } else {

        // Handle unknown commands
        char unknownCmdStr[50];
        snprintf(unknownCmdStr, sizeof(unknownCmdStr), "Unknown Command: %.4s\r\n", command);
        HAL_UART_Transmit(&huart1, (uint8_t*)unknownCmdStr, strlen(unknownCmdStr), HAL_MAX_DELAY);
    }
}

/*
void receiveFile(const char *fileType) {
    // Notify that file reception will start
    char startStr[50];
    snprintf(startStr, sizeof(startStr), "Receiving %s file. Send the file.\r\n", fileType);
    HAL_UART_Transmit(&huart1, (uint8_t*)startStr, strlen(startStr), HAL_MAX_DELAY);

    // Receive file data
    uint8_t fileBuffer[1024];
    uint32_t bytesReceived = 0;

    while (1) {
        HAL_StatusTypeDef status = HAL_UART_Receive(&huart1, fileBuffer + bytesReceived, 1, HAL_MAX_DELAY);
        if (status == HAL_OK) {
            bytesReceived++;
            // Add logic to save fileBuffer to memory or storage
        } else {
            // Handle receive error
            break;
        }

        // Add condition to break the loop when the entire file is received
        if (bytesReceived == sizeof(fileBuffer)) {
            break;
        }
    }

    // Process the received file based on its type
    if (strcmp(fileType, "firmware") == 0) {
        // Notify completion of firmware reception
        char completeStr[] = "Firmware received. Restarting...\r\n";
        HAL_UART_Transmit(&huart1, (uint8_t*)completeStr, strlen(completeStr), HAL_MAX_DELAY);
        NVIC_SystemReset(); // Reset the MCU to use the new firmware
    } else if (strcmp(fileType, "library") == 0) {
        // Store the library file at a specific memory address
        // Add code to store the file to a specific flash memory location
        char completeStr[] = "Library file received and stored.\r\n";
        HAL_UART_Transmit(&huart1, (uint8_t*)completeStr, strlen(completeStr), HAL_MAX_DELAY);
    }
}

*/

void batteryLevel(void) {
	uint16_t raw;

	char adcMsg[8];

	HAL_ADC_Start(&hadc4);
	HAL_ADC_PollForConversion(&hadc4, HAL_MAX_DELAY);
	raw = HAL_ADC_GetValue(&hadc4);
	uint32_t var = (uint32_t) ((raw * 4096) / 3.3);
	uint32_t varPercents = (var * 100) / 370000;
	sprintf(adcMsg, "%lu\r\n", varPercents);
	printf(" battery Level = %s", adcMsg);
	addToDisplayBattery(adcMsg);
	//MESSAGE.Vbatt = adcMsg;
	memcpy(MESSAGE.Vbatt, adcMsg, 2);
}

float readBatteryLevel(void)
{
	//MX_ADC_Init();
	float batteryVoltage, batteryLevel;

	HAL_ADC_Start(&hadc4);

	if (HAL_ADC_PollForConversion(&hadc4, 100) != HAL_OK)
	{
		return 1;
	}

	// Get ADC value
	uint32_t adc_value = HAL_ADC_GetValue(&hadc4);

	// Convert the ADC value to battery percentage
	batteryVoltage = ((adc_value * 2) / 4096.0) * 3.3;
	batteryLevel = (batteryVoltage / 3.7) * 100;

	HAL_ADC_Stop(&hadc4);

	return batteryLevel;
}

void processBroodInfo(void) {

	float temp1 = 0;
	float temp2 = 0;
	float temp3 = 0;

	RTC_CalendarShow(aShowTime, aShowDate);
	MESSAGE.amountOfDays = daysCounter(aShowTime[6], readDay);
	readDay = aShowTime[6];
	temp1 = SHT40_measure_temperature(&hi2c2);
	temp2 = SHT40_measure_temperature(&hi2c4);
	temp3 = SHT40_measure_temperature(&hi2c5);

	currentBroodingState = broodingState(temp1,temp2,temp3);

	if (currentBroodingState == noBrood) {
		addToDisplay(1, "NEPERO");
		printf("Nepero %d dienas\r\n", MESSAGE.amountOfDays);
		beeFrame(close);
	} else if (currentBroodingState == littleBrood) {
		addToDisplay(1, "PERO MAZ");
		printf("Pero maz %d dienas\r\n", MESSAGE.amountOfDays);
		beeFrame(close);
	} else if (currentBroodingState == lotOfBrood) {
		addToDisplay(1, "PERO DAUDZ");
		printf("Pero daudz %d dienas\r\n", MESSAGE.amountOfDays);

		if (MESSAGE.amountOfDays > 10) {
			beeFrame(open);
		} else {
			beeFrame(close);
		}
	}
}


void getLoraTime(void) {

	printf("lora time%s\r\n", loraTime);
	printf("lora date = %s\r\n", loraDate);
	E5ModuleMsg("AT+RTC\r\n");

	char cmdBuff[5];
    memset(cmdBuff, '0', sizeof(cmdBuff));
    cmdBuff[5] = '\0';


	if (HAL_UART_Receive(&huart3, buff, 25, 1000) == HAL_OK) {

		memcpy(&cmdBuff, &buff, 4);
		memcpy(&loraDate[0], &buff[5], LORA_DATE_SIZE);
		memcpy(&loraTime[0], &buff[15], LORA_TIME_SIZE);
		printf("%s", cmdBuff);
		if (strcmp(cmdBuff, "+RTC") == 0)

		{

			memcpy(&loraDate[0], &buff[5], LORA_DATE_SIZE);
			memcpy(&loraTime[0], &buff[15], LORA_TIME_SIZE);
		//	addToDisplay(2, loraDate);
			//addToDisplay(2, loraTime);
			printf("lora time”%s\r\n", loraTime);

			printf("lora date = %s\r\n", loraDate);


		}

	}
}

void setLed(uint8_t led, uint8_t state) {


	switch (led) {
	case ledSad:
		if (state == ON) {
			HAL_GPIO_WritePin(LED_SAD_Port, LED_SAD_Pin, GPIO_PIN_RESET);
		} else {
			HAL_GPIO_WritePin(LED_SAD_Port, LED_SAD_Pin, GPIO_PIN_SET);
		}
		break;
	case ledNeutral:
		if (state == ON) {
			HAL_GPIO_WritePin(LED_NEUTRAL_Port, LED_NEUTRAL_Pin, GPIO_PIN_RESET);
		} else {
			HAL_GPIO_WritePin(LED_NEUTRAL_Port, LED_NEUTRAL_Pin, GPIO_PIN_SET);
		}
		break;
	case ledSmile:
		if (state == ON) {
			HAL_GPIO_WritePin(LED_SMILE_Port, LED_SMILE_Pin, GPIO_PIN_RESET);
		} else {
			HAL_GPIO_WritePin(LED_SMILE_Port, LED_SMILE_Pin, GPIO_PIN_SET);
		}
		break;

	case ledUser:
		if (state == ON) {
			HAL_GPIO_WritePin(LED_USER_Port, LED_USER_Pin, GPIO_PIN_SET);
		} else {
			HAL_GPIO_WritePin(LED_USER_Port, LED_USER_Pin, GPIO_PIN_RESET);
		}
		break;
	case ledPower:
		if (state == ON) {
			HAL_GPIO_WritePin(LED_POWER_Port, LED_POWER_Pin, GPIO_PIN_SET);
		} else {
			HAL_GPIO_WritePin(LED_POWER_Port, LED_POWER_Pin, GPIO_PIN_RESET);
		}
		break;
	case ledWarning:
		if (state == ON) {
			HAL_GPIO_WritePin(LED_WARNING_Port, LED_WARNING_Pin, GPIO_PIN_SET);
		} else {
			HAL_GPIO_WritePin(LED_WARNING_Port, LED_WARNING_Pin, GPIO_PIN_RESET);
		}
		break;
	default:

			HAL_GPIO_WritePin(LED_SAD_Port, LED_SAD_Pin, GPIO_PIN_RESET);

	}
}

void receiveAndProcess(void) {
    // Initialize the command buffer
    memset(cmdBuffer, 0, sizeof(cmdBuffer));

    // Start UART reception in interrupt mode
    HAL_UART_Receive_IT(&huart1, cmdBuffer, CMD_BUFFER_SIZE);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
    if (huart->Instance == USART1) {
        // Null-/hterminate the received command string
        cmdBuffer[CMD_BUFFER_SIZE] = '\0';

        // Process the received command
        processCommand((char*)cmdBuffer);

        // Re-enable UART1 receive interrupt for the next command
        HAL_UART_Receive_IT(&huart1, cmdBuffer, CMD_BUFFER_SIZE);
    }
}

void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin) {
    
	currentMillis = HAL_GetTick();

	switch (GPIO_Pin) {
	case BTN_SAD_Pin:

		if (currentMillis - previousMillis > BTN_DEBOUNCE_TIME) {
			printf("--==Press SAD==--\r\n");
            setLed(ledSad, ON);
            setLed(ledNeutral, OFF);
            setLed(ledSmile, OFF);
            MESSAGE.hiveRate = 1;

			previousMillis = currentMillis;
		}
		break;
	case BTN_NEUTRAL_Pin:

		if (currentMillis - previousMillis > BTN_DEBOUNCE_TIME) {
			// Change the indicator LED register

			//	saveStateInFlash(((uint32_t)0x0803F800), led_neutral);

			printf("--==Press NEUTRAL==--\r\n");
            setLed(ledSad, OFF);
            setLed(ledNeutral, ON);
            setLed(ledSmile, OFF);
            MESSAGE.hiveRate = 2;
			previousMillis = currentMillis;
		}
		break;
	case BTN_SMILE_Pin:

		if (currentMillis - previousMillis > BTN_DEBOUNCE_TIME) {

			printf("--==Press SMILE==--\r\n");
            setLed(ledSad, OFF);
            setLed(ledNeutral, OFF);
            setLed(ledSmile, ON);
            MESSAGE.hiveRate = 3;
			previousMillis = currentMillis;
		}
		break;
	case BTN_WRN_Pin:

		if (currentMillis - previousMillis > BTN_DEBOUNCE_TIME) {

			//HAL_Delay(10);
			//printf("Press duration Count: %lu\r\n",pressDuration);
			warning_state = !warning_state;
		//	MESSAGE.warningState = warning_state;
			printf("--==Press WARNING==--\r\n");
			printf("--==Warning State = %d\r\n",warning_state);
			if (warning_state == 1) {
				printf("--==WARNING = 1 ==--\r\n");
            setLed(ledWarning, ON);
			} else {
				printf("--==WARNING = 0 ==--\r\n");
            setLed(ledWarning, OFF);
			}

			printf("--== WARNING ==--\r\n");
			//warning_state = 0;
			//longPressOccured = 1;
			previousMillis = currentMillis;
		}
		break;

	case ACC_INT_Pin:
		motionState = 1;
		MESSAGE.motionState = 1;
		printf("--== Motion detected ==--\r\n");
		MX_I2C2_Init();
		MC3630INThandler(&hi2c2, &evt_mc36xx);
		MC3630stop(&hi2c2);
		MC3630setSniffThreshold(&hi2c2, MC36XX_AXIS_X, 2);
		MC3630setSniffThreshold(&hi2c2, MC36XX_AXIS_Y, 2);
		MC3630setSniffThreshold(&hi2c2, MC36XX_AXIS_Z, 2);
		MC3630setSniffDetectCount(&hi2c2, MC36XX_AXIS_X, 1);
		MC3630setSniffDetectCount(&hi2c2, MC36XX_AXIS_Y, 1);
		MC3630setSniffDetectCount(&hi2c2, MC36XX_AXIS_Z, 1);
		MC3630setSniffAndOrN(&hi2c2, MC36XX_ANDORN_OR);
		MC3630setSniffDeltaMode(&hi2c2, MC36XX_DELTA_MODE_C2P);
		MC3630setINTCtrl(&hi2c2, 0, 0, 0, 0, 1);
		MC3630sniff(&hi2c2);
		break;

	case MICROSWITCH_2:
		if (currentMillis - previousMillis > BTN_DEBOUNCE_TIME) {

			printf("Microswitch 2 interupt\r\n");

		}
		break;
	default:
		break;
	}
}



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_GPDMA1_Init();
  MX_ADC4_Init();
  MX_DAC1_Init();
  MX_FLASH_Init();
  MX_I2C2_Init();
  MX_RTC_Init();
  MX_SAI1_Init();
  MX_SPI1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_TIM6_Init();
  MX_TIM15_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
Audio_Init();
//	SSD1306_Init(); // initialize the display
	SHT40_Init();
	DS18B20_Init();

	EEPROM_Init(&hi2c2);
	W25qxx_Init();
        initRTC();
        configureRTCForDayTime();
		msg message;
        message.temp1 = 7.7,
        message.temp2 = 15.3,
        message.temp3 = 20.5,
        message.humidity = 55.0,
        message.broodingState = 1,
        message.broodingQuality = 2,
        message.incomingState = 3,
        message.swarmingState = 4,
        message.hungerState = 5,
        message.queenlessState = 6,
        message.hiveRate = 7,
        message.motionState = 8,
        message.frameState = 9,
        message.frameScript = 10,
        message.alarmState = 11,
        message.warningState = 12,
        message.userLedState = 13,
        message.amountOfDays = 100;
	printf("Init Done\n");
	HAL_TIM_Base_Start(&htim6);
	float val = 1.2;
	uint32_t var;
	setLed(ledPower, ON);
	addToDisplay(1, "Start");
	//uint32_t var = (uint32_t)(val*4096)/3.3;
	// Activate_DAC();
//	W25qxx_EraseChip();

	addToDisplay(1, "RUNING");
	printf("Testss-----------------------------------------------------------\r\n");

        float sht40Temp = SHT40_measure_temperature(&hi2c2);

		printf("SHT40 Temperature: %.2f degrees Celsius\n", sht40Temp);
	float ds18b20Temp1 =  DS18B20_ReadTemp(SENSOR_1);
        printf("DS18B20 Temperature: %.2f degrees Celsius\n", ds18b20Temp1);
	float ds18b20Temp2 = DS18B20_ReadTemp(SENSOR_2);
        printf("DS18B20 Temperature: %.2f degrees Celsius\n", ds18b20Temp2);
	float ds18b20Temp3 = DS18B20_ReadTemp(SENSOR_3);
        printf("DS18B20 Temperature: %.2f degrees Celsius\n", ds18b20Temp3);
	float ds18b20Temp4 = DS18B20_ReadTemp(SENSOR_4);
        printf("DS18B20 Temperature: %.2f degrees Celsius\n", ds18b20Temp4);
	float ds18b20Temp5 = DS18B20_ReadTemp(SENSOR_5);
        printf("DS18B20 Temperature: %.2f degrees Celsius\n", ds18b20Temp5);
	float ds18b20Temp6 = DS18B20_ReadTemp(SENSOR_6);
        printf("DS18B20 Temperature: %.2f degrees Celsius\n", ds18b20Temp6);
	float ds18b20Temp7 = DS18B20_ReadTemp(SENSOR_7);
        printf("DS18B20 Temperature: %.2f degrees Celsius\n", ds18b20Temp7);
	float ds18b20Temp8 = DS18B20_ReadTemp(SENSOR_8);
        printf("DS18B20 Temperature: %.2f degrees Celsius\n", ds18b20Temp8);







	char adcMsg2[8];
	uint8_t s = 0;
        receiveAndProcess();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

		if (halfBuffer == 1)
		{
			processAudioData(audio_rx);
			halfBuffer = 0;
			//printf("-=halfBuffer=-\n\r" );
		}

		if (fullBuffer == 1)
		{
			processAudioData(audio_rx + (AUDIO_BUFFER_SIZE/2));
			fullBuffer = 0;
			//printf("-=FuulBuffer=-\r\n" );
		}

	//	printf("-=HalfBuffer Count = %d \r\n", halfBufferCount);
		// Check if certain number of cycles has been processed
		if (halfBufferCount >= TOTAL_BUFFERS)
		{
			//stop DMA I2S
		//	printf("Audi Buffering Done");
				HAL_SAI_DMAStop(&hsai_BlockB1);
		}
//	}

///	if (EEPROM_Read(&hi2c2, 1000, (uint8_t*)rawAudioDataint, sizeof(rawAudioDataint), 0) != HAL_OK)
//	{
//		return;
//	}

//	for(size_t i = 0; i < RAW_BUFFER_SIZE; i++) {
//		rawAudioDatafloat[i] = (float)rawAudioDataint[i];
//	}





//			if (halfBufferCount >= TOTAL_BUFFERS) {
//				HAL_SAI_DMAStop(&hsai_BlockB1);
//                _send_audio_data();

//				halfBufferCount = 0;
//			}
/*


   if (halfBuffer) {
            halfBuffer = 0;
			j++;
            // Process first half of the buffer
            for (int i = 0; i < AUDIO_BUFFER_SIZE / 2; i++) {
                uartBuffer[i * 2] = (audio_rx[i] & 0xFF00) >> 8;
                uartBuffer[i * 2 + 1] = audio_rx[i] & 0x00FF;
            }
            HAL_UART_Transmit(&huart1, uartBuffer, AUDIO_BUFFER_SIZE, 100);
        }

        if (fullBuffer) {
            fullBuffer = 0;
            j++;
            // Process second half of the buffer
            for (int i = AUDIO_BUFFER_SIZE / 2; i < AUDIO_BUFFER_SIZE; i++) {
                uartBuffer[(i - AUDIO_BUFFER_SIZE / 2) * 2] = (audio_rx[i] & 0xFF00) >> 8;
                uartBuffer[(i - AUDIO_BUFFER_SIZE / 2) * 2 + 1] = audio_rx[i] & 0x00FF;
            }
            HAL_UART_Transmit(&huart1, uartBuffer, AUDIO_BUFFER_SIZE, 100);
       

    }


//			Process_AudioData();
		//	printf("J++");
			if (j == AUDIO_BUFFER_SIZE) {
				j=0;
				printf("Done Audio Procesing\r\n");
				if (HAL_SAI_DMAStop(&hsai_BlockB1) != HAL_OK) {

				Error_Handler();
			}    


			}

			*/



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

setLed(ledUser, ON);
		//sleepWithWakeupSources;
	    
		MESSAGE.motionState = 2;
		//dacPlay(soundata_data, soundata_length);
		//HAL_Delay(5000);
		//dacPlay(pipping, SIZE_OF_PIPPING);
		//batteryLevel();
	//	sprintf(adcMsg2, "%2.2f", readBatteryLevel());
	//	printf(" battery Level = %s", adcMsg2);
	//	addToDisplayBattery(adcMsg2);
		//MESSAGE.Vbatt = adcMsg;
	//	memcpy(MESSAGE.Vbatt, adcMsg2, 2);

		//MESSAGE.temp3 = readBatteryLevel;
		//getLoraTime();
//		E5ModuleTime("AT+RTC\r\n");
//		processBroodInfo();
		//listenE5Lora();
		//processAudio();
		//dacPlay(divaam, SIZE_OF_DIVAAM);
//		sendLora(MESSAGE);
		MESSAGE.motionState = 0;
		setLed(ledUser, OFF);
	//	HAL_Delay(5000);



  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_4;
  RCC_OscInitStruct.LSIDiv = RCC_LSI_DIV1;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLMBOOST = RCC_PLLMBOOST_DIV1;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 80;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLLVCIRANGE_0;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_PCLK3;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV8;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC4_Init(void)
{

  /* USER CODE BEGIN ADC4_Init 0 */

  /* USER CODE END ADC4_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC4_Init 1 */

  /* USER CODE END ADC4_Init 1 */

  /** Common config
  */
  hadc4.Instance = ADC4;
  hadc4.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc4.Init.Resolution = ADC_RESOLUTION_12B;
  hadc4.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc4.Init.ScanConvMode = ADC4_SCAN_DISABLE;
  hadc4.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc4.Init.LowPowerAutoPowerOff = ADC_LOW_POWER_NONE;
  hadc4.Init.LowPowerAutoWait = DISABLE;
  hadc4.Init.ContinuousConvMode = DISABLE;
  hadc4.Init.NbrOfConversion = 1;
  hadc4.Init.DiscontinuousConvMode = DISABLE;
  hadc4.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc4.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc4.Init.DMAContinuousRequests = DISABLE;
  hadc4.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_LOW;
  hadc4.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc4.Init.SamplingTimeCommon1 = ADC4_SAMPLETIME_1CYCLE_5;
  hadc4.Init.SamplingTimeCommon2 = ADC4_SAMPLETIME_1CYCLE_5;
  hadc4.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc4) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = ADC4_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC4_SAMPLINGTIME_COMMON_1;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc4, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC4_Init 2 */

  /* USER CODE END ADC4_Init 2 */

}

/**
  * @brief DAC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC1_Init(void)
{

  /* USER CODE BEGIN DAC1_Init 0 */

  /* USER CODE END DAC1_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};
  DAC_AutonomousModeConfTypeDef sAutonomousMode = {0};

  /* USER CODE BEGIN DAC1_Init 1 */

  /* USER CODE END DAC1_Init 1 */

  /** DAC Initialization
  */
  hdac1.Instance = DAC1;
  if (HAL_DAC_Init(&hdac1) != HAL_OK)
  {
    Error_Handler();
  }

  /** DAC channel OUT1 config
  */
  sConfig.DAC_HighFrequency = DAC_HIGH_FREQUENCY_INTERFACE_MODE_DISABLE;
  sConfig.DAC_DMADoubleDataMode = DISABLE;
  sConfig.DAC_SignedFormat = DISABLE;
  sConfig.DAC_SampleAndHold = DAC_SAMPLEANDHOLD_DISABLE;
  sConfig.DAC_Trigger = DAC_TRIGGER_SOFTWARE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  sConfig.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_BOTH;
  sConfig.DAC_UserTrimming = DAC_TRIMMING_FACTORY;
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Autonomous Mode
  */
  sAutonomousMode.AutonomousModeState = DAC_AUTONOMOUS_MODE_DISABLE;
  if (HAL_DACEx_SetConfigAutonomousMode(&hdac1, &sAutonomousMode) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC1_Init 2 */

  /* USER CODE END DAC1_Init 2 */

}

/**
  * @brief FLASH Initialization Function
  * @param None
  * @retval None
  */
static void MX_FLASH_Init(void)
{

  /* USER CODE BEGIN FLASH_Init 0 */

  /* USER CODE END FLASH_Init 0 */

  /* USER CODE BEGIN FLASH_Init 1 */

  /* USER CODE END FLASH_Init 1 */
  if (HAL_FLASH_Unlock() != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_FLASH_Lock() != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN FLASH_Init 2 */

  /* USER CODE END FLASH_Init 2 */

}

/**
  * @brief GPDMA1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPDMA1_Init(void)
{

  /* USER CODE BEGIN GPDMA1_Init 0 */

  /* USER CODE END GPDMA1_Init 0 */

  /* Peripheral clock enable */
  __HAL_RCC_GPDMA1_CLK_ENABLE();

  /* GPDMA1 interrupt Init */
    HAL_NVIC_SetPriority(GPDMA1_Channel0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(GPDMA1_Channel0_IRQn);

  /* USER CODE BEGIN GPDMA1_Init 1 */

  /* USER CODE END GPDMA1_Init 1 */
  /* USER CODE BEGIN GPDMA1_Init 2 */

  /* USER CODE END GPDMA1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x00100822;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief I2C4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C4_Init(void)
{

  /* USER CODE BEGIN I2C4_Init 0 */

  /* USER CODE END I2C4_Init 0 */

  /* USER CODE BEGIN I2C4_Init 1 */

  /* USER CODE END I2C4_Init 1 */
  hi2c4.Instance = I2C4;
  hi2c4.Init.Timing = 0x00100822;
  hi2c4.Init.OwnAddress1 = 0;
  hi2c4.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c4.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c4.Init.OwnAddress2 = 0;
  hi2c4.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c4.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c4.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c4) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c4, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c4, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C4_Init 2 */

  /* USER CODE END I2C4_Init 2 */

}

/**
  * @brief I2C5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C5_Init(void)
{

  /* USER CODE BEGIN I2C5_Init 0 */

  /* USER CODE END I2C5_Init 0 */

  /* USER CODE BEGIN I2C5_Init 1 */

  /* USER CODE END I2C5_Init 1 */
  hi2c5.Instance = I2C5;
  hi2c5.Init.Timing = 0x00100822;
  hi2c5.Init.OwnAddress1 = 0;
  hi2c5.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c5.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c5.Init.OwnAddress2 = 0;
  hi2c5.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c5.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c5.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c5) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c5, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c5, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C5_Init 2 */

  /* USER CODE END I2C5_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_PrivilegeStateTypeDef privilegeState = {0};
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  hrtc.Init.OutPutPullUp = RTC_OUTPUT_PULLUP_NONE;
  hrtc.Init.BinMode = RTC_BINARY_NONE;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }
  privilegeState.rtcPrivilegeFull = RTC_PRIVILEGE_FULL_NO;
  privilegeState.backupRegisterPrivZone = RTC_PRIVILEGE_BKUP_ZONE_NONE;
  privilegeState.backupRegisterStartZone2 = RTC_BKP_DR0;
  privilegeState.backupRegisterStartZone3 = RTC_BKP_DR0;
  if (HAL_RTCEx_PrivilegeModeSet(&hrtc, &privilegeState) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

  /** Enable the WakeUp
  */
  if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 500, RTC_WAKEUPCLOCK_RTCCLK_DIV16, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief SAI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SAI1_Init(void)
{

  /* USER CODE BEGIN SAI1_Init 0 */

  /* USER CODE END SAI1_Init 0 */

  /* USER CODE BEGIN SAI1_Init 1 */

  /* USER CODE END SAI1_Init 1 */
  hsai_BlockB1.Instance = SAI1_Block_B;
  hsai_BlockB1.Init.AudioMode = SAI_MODEMASTER_RX;
  hsai_BlockB1.Init.Synchro = SAI_ASYNCHRONOUS;
  hsai_BlockB1.Init.OutputDrive = SAI_OUTPUTDRIVE_DISABLE;
  hsai_BlockB1.Init.NoDivider = SAI_MASTERDIVIDER_ENABLE;
  hsai_BlockB1.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_EMPTY;
  hsai_BlockB1.Init.AudioFrequency = SAI_AUDIO_FREQUENCY_16K;
  hsai_BlockB1.Init.SynchroExt = SAI_SYNCEXT_DISABLE;
  hsai_BlockB1.Init.MckOutput = SAI_MCK_OUTPUT_DISABLE;
  hsai_BlockB1.Init.MonoStereoMode = SAI_MONOMODE;
  hsai_BlockB1.Init.CompandingMode = SAI_NOCOMPANDING;
  if (HAL_SAI_InitProtocol(&hsai_BlockB1, SAI_I2S_STANDARD, SAI_PROTOCOL_DATASIZE_16BIT, 2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SAI1_Init 2 */

  /* USER CODE END SAI1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  SPI_AutonomousModeConfTypeDef HAL_SPI_AutonomousMode_Cfg_Struct = {0};

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_4BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 0x7;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  hspi1.Init.NSSPolarity = SPI_NSS_POLARITY_LOW;
  hspi1.Init.FifoThreshold = SPI_FIFO_THRESHOLD_01DATA;
  hspi1.Init.MasterSSIdleness = SPI_MASTER_SS_IDLENESS_00CYCLE;
  hspi1.Init.MasterInterDataIdleness = SPI_MASTER_INTERDATA_IDLENESS_00CYCLE;
  hspi1.Init.MasterReceiverAutoSusp = SPI_MASTER_RX_AUTOSUSP_DISABLE;
  hspi1.Init.MasterKeepIOState = SPI_MASTER_KEEP_IO_STATE_DISABLE;
  hspi1.Init.IOSwap = SPI_IO_SWAP_DISABLE;
  hspi1.Init.ReadyMasterManagement = SPI_RDY_MASTER_MANAGEMENT_INTERNALLY;
  hspi1.Init.ReadyPolarity = SPI_RDY_POLARITY_HIGH;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_SPI_AutonomousMode_Cfg_Struct.TriggerState = SPI_AUTO_MODE_DISABLE;
  HAL_SPI_AutonomousMode_Cfg_Struct.TriggerSelection = SPI_GRP1_GPDMA_CH0_TCF_TRG;
  HAL_SPI_AutonomousMode_Cfg_Struct.TriggerPolarity = SPI_TRIG_POLARITY_RISING;
  if (HAL_SPIEx_SetConfigAutonomousMode(&hspi1, &HAL_SPI_AutonomousMode_Cfg_Struct) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 800;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.BreakAFMode = TIM_BREAK_AFMODE_INPUT;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.Break2AFMode = TIM_BREAK_AFMODE_INPUT;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 800;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 800;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 800;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 1000;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 800;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 1000;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */
  HAL_TIM_MspPostInit(&htim5);

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 50;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 100;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM15 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM15_Init(void)
{

  /* USER CODE BEGIN TIM15_Init 0 */

  /* USER CODE END TIM15_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM15_Init 1 */

  /* USER CODE END TIM15_Init 1 */
  htim15.Instance = TIM15;
  htim15.Init.Prescaler = 800;
  htim15.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim15.Init.Period = 1000;
  htim15.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim15.Init.RepetitionCounter = 0;
  htim15.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim15) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim15, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim15) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim15, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim15, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim15, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM15_Init 2 */

  /* USER CODE END TIM15_Init 2 */
  HAL_TIM_MspPostInit(&htim15);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 9600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart3, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart3, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin : PE2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);


  /*Configure GPIO pin : PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


  /*Configure GPIO pin : PC0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PC1 PC4 PC5 PC6
                           PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB1 PB6 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PE7 PE11 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PD8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);



  /*Configure GPIO pins : PD12 PD14 */
  GPIO_InitStruct.Pin = GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PA11 */
  GPIO_InitStruct.Pin = GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);




  /*Configure GPIO pin : PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI6_IRQn);

  HAL_NVIC_SetPriority(EXTI7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI7_IRQn);

  HAL_NVIC_SetPriority(EXTI11_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI11_IRQn);

  HAL_NVIC_SetPriority(EXTI12_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI12_IRQn);

  HAL_NVIC_SetPriority(EXTI14_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI14_IRQn);

/* USER CODE BEGIN MX_GPIO_Init_2 */

  
  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_2, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET);

/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
static void RTC_CalendarShow(uint8_t *showtime, uint8_t *showdate) {
	RTC_DateTypeDef sdatestructureget;
	RTC_TimeTypeDef stimestructureget;

	/* Get the RTC current Time */
	HAL_RTC_GetTime(&hrtc, &stimestructureget, RTC_FORMAT_BIN);
	/* Get the RTC current Date */
	HAL_RTC_GetDate(&hrtc, &sdatestructureget, RTC_FORMAT_BIN);
	/* Display time Format : hh:mm:ss */
	sprintf((char*) showtime, "%2d:%2d:%2d", stimestructureget.Hours,
			stimestructureget.Minutes, stimestructureget.Seconds);
	/* Display date Format : mm-dd-yy */
	sprintf((char*) showdate, "%2d-%2d-%2d", 2000 + sdatestructureget.Year,
			sdatestructureget.Month, sdatestructureget.Date);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,*/
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
