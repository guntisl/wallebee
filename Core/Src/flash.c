// flash.c

#include "flash.h"
#include <string.h>  // For memcpy

FLASH_EraseInitTypeDef EraseInitStruct;
uint32_t PageError = 0;

uint8_t crc8(const uint8_t *data, size_t len) {
    uint8_t crc = 0x00;
    for (size_t i = 0; i < len; i++) {
        crc ^= data[i];
        for (uint8_t j = 0; j < 8; j++) {
            if (crc & 0x80) {
                crc = (crc << 1) ^ 0x07;
            } else {
                crc <<= 1;
            }
        }
    }
    return crc;
}

void FLASH_Unlock(void) {
    HAL_FLASH_Unlock();
}

void FLASH_Lock(void) {
    HAL_FLASH_Lock();
}

HAL_StatusTypeDef FLASH_Erase(uint32_t startAddress, uint32_t endAddress) {
    uint32_t FirstPage = 0, NbOfPages = 0;

    FirstPage = (startAddress - FLASH_BASE) / FLASH_PAGE_SIZE;
    NbOfPages = ((endAddress - startAddress) / FLASH_PAGE_SIZE) + 1;

    EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.Banks = FLASH_BANK_1;
    EraseInitStruct.Page = FirstPage;
    EraseInitStruct.NbPages = NbOfPages;

    return HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
}

HAL_StatusTypeDef FLASH_Write(uint32_t address, uint8_t *data, uint32_t length) {
    HAL_StatusTypeDef status;
    uint32_t i;

    for (i = 0; i < length; i += 8) {  // STM32U5 FLASH writes in 64-bit words
        uint64_t data_word = 0;
        memcpy(&data_word, &data[i], 8);
        status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_QUADWORD, address + i, data_word);
        if (status != HAL_OK) {
            return status;
        }
    }
    return HAL_OK;
}
