// lora.c

#include "lora.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#include "main.h"


#define RESPONSE_TIMEOUT 5000 // Timeout for response in milliseconds
#define RESPONSE_BUFFER_SIZE 128 // Buffer size for UART response

uint8_t responseBuffer[RESPONSE_BUFFER_SIZE];


msg MESSAGE = {0};

// Define loraDate and loraTime for storing the date and time received
char loraDate[LORA_DATE_SIZE];
char loraTime[LORA_TIME_SIZE];

void E5ModuleMsg(const char *format, ... ) {
    char buffer[256]; // Buffer for formatted string
    va_list args;

    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer), format, args);
    va_end(args);

   // HAL_UART_Transmit(&huart1, (uint8_t*)buffer, strlen(buffer), HAL_MAX_DELAY);
    HAL_UART_Transmit(&huart3, (uint8_t*)buffer, strlen(buffer), HAL_MAX_DELAY);

    uint8_t rxBuffer[256];
    uint16_t rxIndex = 0;
/*
    while (1) {
        uint8_t rxChar;
        if (HAL_UART_Receive(&huart3, &rxChar, 1, HAL_MAX_DELAY) == HAL_OK) {
            rxBuffer[rxIndex++] = rxChar;

            if (rxChar == '\n' || rxChar == '\r') {
                rxBuffer[rxIndex] = '\0';
                HAL_UART_Transmit(&huart1, rxBuffer, rxIndex, HAL_MAX_DELAY);
                rxIndex = 0;
            }
        }
    }  */
}

void E5ModuleTime(const char *format, ... ) {
    char buffer[256]; // Buffer for formatted string
    va_list args;

    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer), format, args);
    va_end(args);

    HAL_UART_Transmit(&huart1, (uint8_t*)buffer, strlen(buffer), HAL_MAX_DELAY);
    HAL_UART_Transmit(&huart3, (uint8_t*)buffer, strlen(buffer), HAL_MAX_DELAY);

    uint8_t rxBuffer[256];
    uint16_t rxIndex = 0;

    while (1) {
        uint8_t rxChar;
        if (HAL_UART_Receive(&huart3, &rxChar, 1, HAL_MAX_DELAY) == HAL_OK) {
            rxBuffer[rxIndex++] = rxChar;

            if (rxChar == '\n' || rxChar == '\r') {
                rxBuffer[rxIndex] = '\0';

                memcpy(&loraDate[0], &rxBuffer[5], LORA_DATE_SIZE);
                memcpy(&loraTime[0], &rxBuffer[15], LORA_TIME_SIZE);

                HAL_UART_Transmit(&huart1, rxBuffer, rxIndex, HAL_MAX_DELAY);
                printf("lora time = %s\r\n", loraTime);
                printf("lora date = %s\r\n", loraDate);
                rxIndex = 0;
            }
        }
    }
}

void setPayloadAndSend(const msg *message)
{
    // Ensure message is not NULL
    if (message == NULL) {
        return;
    }

    // AT command prefix
    const char *atCommand = "AT+MSG=";
    size_t prefixLength = strlen(atCommand);

    uint8_t payload[23];
    const char payloadString[200];
    // Convert temperatures to centidegrees and store in payload
    int16_t tempInCentidegrees1 = (int16_t)(message->temp1 * 100.0);
    int16_t tempInCentidegrees2 = (int16_t)(message->temp2 * 100.0);
    int16_t tempInCentidegrees3 = (int16_t)(message->temp3 * 100.0);

    // Packing the payload array with data
    payload[0] = (uint8_t)(tempInCentidegrees1 & 0xFF);        // Temp1 lower byte
    payload[1] = (uint8_t)((tempInCentidegrees1 >> 8) & 0xFF); // Temp1 upper byte

    payload[2] = (uint8_t)(tempInCentidegrees2 & 0xFF);        // Temp2 lower byte
    payload[3] = (uint8_t)((tempInCentidegrees2 >> 8) & 0xFF); // Temp2 upper byte

    payload[4] = (uint8_t)(tempInCentidegrees3 & 0xFF);        // Temp3 lower byte
    payload[5] = (uint8_t)((tempInCentidegrees3 >> 8) & 0xFF); // Temp3 upper byte

    int16_t humidityInCentipercent = (int16_t)(message->humidity * 100.0);
    payload[6] = (uint8_t)(humidityInCentipercent & 0xFF);     // Humidity lower byte
    payload[7] = (uint8_t)((humidityInCentipercent >> 8) & 0xFF); // Humidity upper byte

    // Remaining single byte states
    payload[8] = message->broodingState;
    payload[9] = message->broodingQuality;
    payload[10] = message->incomingState;
    payload[11] = message->swarmingState;
    payload[12] = message->hungerState;
    payload[13] = message->queenlessState;
    payload[14] = message->hiveRate;
    payload[15] = message->motionState;
    payload[16] = message->frameState;
    payload[17] = message->frameScript;
    payload[18] = message->alarmState;
    payload[19] = message->warningState;
    payload[20] = message->userLedState;

    // Split amountOfDays into two bytes and store
    payload[21] = (uint8_t)(message->amountOfDays & 0xFF);        // amountOfDays lower byte
    payload[22] = (uint8_t)((message->amountOfDays >> 8) & 0xFF); // amountOfDays upper byte

    // Total message length
    size_t totalMessageLength = prefixLength + sizeof(payload);

    // Allocate buffer for complete message
    uint8_t completeMessage[totalMessageLength];

    // Copy AT command and payload into the complete message buffer
    memcpy(completeMessage, atCommand, prefixLength);
    memcpy(completeMessage + prefixLength, payload, sizeof(payload));

    snprintf(payloadString, sizeof(payloadString), "AT+MSG=%u",payload);
    HAL_UART_Transmit(&huart1, (uint8_t*)payloadString, strlen(payloadString), HAL_MAX_DELAY);
    HAL_UART_Transmit(&huart3, (uint8_t*)payloadString, strlen(payloadString), HAL_MAX_DELAY);

    // Send the complete message over UART3
    //  HAL_UART_Transmit(&huart3, completeMessage, totalMessageLength, HAL_MAX_DELAY);
    HAL_UART_Transmit(&huart1, (uint8_t *) atCommand, 7 , HAL_MAX_DELAY);
    HAL_UART_Transmit(&huart1,  (uint8_t *) payload, sizeof(payload), HAL_MAX_DELAY);
}


void loraJoin(void) {
	// Example use of sendATCommand to join a LoRa network
	E5ModuleMsg("AT+RST");
	HAL_Delay(1000); // Wait for reset
	E5ModuleMsg("AT+MODE=LWOTAA");
	E5ModuleMsg("AT+DR=EU868");
	E5ModuleMsg("AT+ID=DevEui,\""DEV_EUI"\"");
	E5ModuleMsg("AT+ID=AppEui,\"" APP_EUI "\"");
	E5ModuleMsg("ATAT+KEY=APPKEY,\"" APPS_S_KEY "\"");
	E5ModuleMsg("AT+KEY=APPKEY,\"" NW_KEY "\"");
	E5ModuleMsg("AT+JOIN");

	// After joining, send a message over LoRa
	E5ModuleMsg("AT+CMSGHEX=2525252525252525252525252525");

	// Notify that joining and messaging is done
	char doneMessage[] = "Done\r\n";
	HAL_UART_Transmit(&huart1, (uint8_t*)doneMessage, strlen(doneMessage), HAL_MAX_DELAY);
}



void sendATCommand(const char *command) {
	// Send the command over UART3
	HAL_UART_Transmit(&huart3, (uint8_t*)command, strlen(command), HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart3, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

	// Also send the command over UART1 (optional, if required)
	HAL_UART_Transmit(&huart1, (uint8_t*)command, strlen(command), HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart1, (uint8_t*)"\r\n", 2, HAL_MAX_DELAY);

	// Clear the response buffer
	memset(responseBuffer, 0, RESPONSE_BUFFER_SIZE);

	// Receive the response from the LoRa module over UART3
	if (HAL_UART_Receive(&huart3, responseBuffer, RESPONSE_BUFFER_SIZE, RESPONSE_TIMEOUT) == HAL_OK) {
		// Transmit the received response over UART1
		HAL_UART_Transmit(&huart1, responseBuffer, strlen((char*)responseBuffer), HAL_MAX_DELAY);
	} else {
		// No response received, handle timeout or error as needed
		char errorMsg[] = "No response or timeout reached.\r\n";
		HAL_UART_Transmit(&huart1, (uint8_t*)errorMsg, strlen(errorMsg), HAL_MAX_DELAY);
	}
}

void E5SendingMSG(msg MESSAGE) {
	char dataString[256];
	char commandString[300];
	E5ModuleMsg("AT+CMSGHEX+MSG=\"%c%.2f%.2f%.2f%.2f%u%d%u%u%u%u%u%u%u%u%u%u%u%u\"",
			MESSAGE.Vbatt[0],
			MESSAGE.temp1,
			MESSAGE.temp2,
			MESSAGE.temp3,
			MESSAGE.humidity,
			MESSAGE.broodingState,
			MESSAGE.broodingQuality,
			MESSAGE.incomingState,
			MESSAGE.swarmingState,
			MESSAGE.hungerState,
			MESSAGE.queenlessState,
			MESSAGE.hiveRate,
			MESSAGE.motionState,
			MESSAGE.frameState,
			MESSAGE.frameScript,
			MESSAGE.alarmState,
			MESSAGE.warningState,
			MESSAGE.userLedState,
			MESSAGE.amountOfDays);

	printf("Done MSG\n\r");
}
