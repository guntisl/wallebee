/*
 * mc3630.c
 *
 *  Created on: 2023. gada 15. jūn.
 *      Author: oskars_selis
 */

#include <mc3630.h>

// ===============================================================================================================
// Communication Protocol connection check
// ===============================================================================================================

// Check if the I2C device is ready
HAL_StatusTypeDef MC3630checkConnection(I2C_HandleTypeDef *hi2c)
{
	return HAL_I2C_IsDeviceReady(hi2c, MC3630_I2C_ADDRESS, 1, HAL_MAX_DELAY);
}

// ===============================================================================================================
// Write and Read data
// ===============================================================================================================

// Read register bit
uint8_t MC3630readRegisterBit(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t pos)
{
	uint8_t value;
	value = MC3630readRegister8(hi2c, reg);
	return ((value >> pos) & 1);
}

// Read 8-bit from register
uint8_t MC3630readRegister8(I2C_HandleTypeDef *hi2c, uint8_t reg)
{
	uint8_t value;

	if (HAL_I2C_Mem_Read(hi2c, MC3630_I2C_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, &value, 1, HAL_MAX_DELAY) != HAL_OK)
	{
		// Handle error here
	}

	return value;
}

// Read 16-bit from register
uint16_t MC3630readRegister16(I2C_HandleTypeDef *hi2c, uint8_t reg)
{
	uint16_t value;

	uint8_t data[2];
	if (HAL_I2C_Mem_Read(hi2c, MC3630_I2C_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, data, 2, HAL_MAX_DELAY) != HAL_OK)
	{
		// Handle error here
	}
	value = (data[0] << 8) | data[1];

	return value;
}

// Repeated Read Byte(s) from register
void MC3630readRegisters(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t *buffer, uint8_t len)
{
	if (HAL_I2C_Mem_Read(hi2c, MC3630_I2C_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, buffer, len, HAL_MAX_DELAY) != HAL_OK)
	{
		// Handle error here
	}
}

// Write register bite
void MC3630writeRegisterBit(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t pos, uint8_t state)
{
	uint8_t value;

	if (HAL_I2C_Mem_Read(hi2c, MC3630_I2C_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, &value, 1, HAL_MAX_DELAY) != HAL_OK)
	{
		// Handle read error here
	}

	if (state)
		value |= (1 << pos);
	else
		value &= ~(1 << pos);

	if (HAL_I2C_Mem_Write(hi2c, MC3630_I2C_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, &value, 1, HAL_MAX_DELAY) != HAL_OK)
	{
		// Handle write error here
	}
}

// Write 8-bit to register
void MC3630writeRegister8(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t value)
{
	HAL_StatusTypeDef status = HAL_I2C_Mem_Write(hi2c, MC3630_I2C_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, &value, 1, HAL_MAX_DELAY);
	if (status != HAL_OK)
	{
		// Handle error here
	}
}

// Write 16-bit to register
void MC3630writeRegister16(I2C_HandleTypeDef *hi2c, uint8_t reg, int16_t value)
{
	uint8_t data[2];
	data[0] = (uint8_t)(value >> 8);
	data[1] = (uint8_t)value;
	if (HAL_I2C_Mem_Write(hi2c, MC3630_I2C_ADDRESS, reg, I2C_MEMADD_SIZE_8BIT, data, 2, HAL_MAX_DELAY) != HAL_OK)
	{
		// Handle error here
	}
}

// ===============================================================================================================
// Sensor initializing
// ===============================================================================================================

uint8_t MC3630start(I2C_HandleTypeDef *hi2c)
{
	MC3630init(hi2c);
	MC3630setMode(hi2c, MC3630_STANDBY_MODE);

	// SetWakeAGAIN
	// SetSniffAGAIN

	// Check I2C connection
	uint8_t deviceID = MC3630readRegister8(hi2c, MC3630_REG_CHIP_ID);
	if (deviceID != 0x71)
	{
		return 1;
	}

	// Set range
	MC3630setRangeCtrl(hi2c, MC3630_RANGE_8G);
	// Set resolution
	MC3630setResolutionCtrl(hi2c, MC3630_RES_8_BITS);
	// Set sampling rate
	MC3630setCWakeSampleRate(hi2c, MC36XX_CWAKE_SR_DEFAULT_54Hz);
	// Set sampling rate
	MC3630setCWakeSampleRate(hi2c, MC36XX_SNIFF_SR_105Hz);

	// Set Mode: Active
	MC3630setMode(hi2c, MC3630_CWAKE_MODE);

	//HAL_Delay(50);

	return 0;
}

//  Initialization Sequence for I2C Interface
void MC3630init(I2C_HandleTypeDef *hi2c)
{
	// Go to standby
	MC3630writeRegister8(hi2c, MC3630_REG_MODE_C, MC3630_STANDBY_MODE);
	//HAL_Delay(10);

	// Reset (or Power-On)
	MC3630writeRegister8(hi2c, MC3630_REG_RESET, MC3630_RESET);
	//HAL_Delay(50);

	// I2C mode enabled
	MC3630writeRegister8(hi2c, MC3630_REG_FREG_1, MC3630_I2C_ENABLE);
	//HAL_Delay(10);

	// Initialization
	MC3630writeRegister8(hi2c, MC3630_REG_INIT_1, MC3630_INIT_1);
	//HAL_Delay(10);

	MC3630writeRegister8(hi2c, MC3630_REG_DMX, MC3630_INIT_DMX);
	//HAL_Delay(10);

	MC3630writeRegister8(hi2c, MC3630_REG_DMY, MC3630_INIT_DMY);
	//HAL_Delay(10);

	MC3630writeRegister8(hi2c, MC3630_REG_INIT_2, MC3630_INIT_2);
	//HAL_Delay(10);

	MC3630writeRegister8(hi2c, MC3630_REG_INIT_3, MC3630_INIT_3);
	//HAL_Delay(50);

	// Go to standby
	MC3630writeRegister8(hi2c, MC3630_REG_MODE_C, MC3630_STANDBY_MODE);
	//HAL_Delay(10);
}

// Set the operation mode
void MC3630setMode(I2C_HandleTypeDef *hi2c, uint8_t mode)
{
	uint8_t value;

	value = MC3630readRegister8(hi2c, MC3630_REG_MODE_C);
	value &= 0b11110000;
	value |= mode;

	MC3630writeRegister8(hi2c, MC3630_REG_INIT_1, MC3630_INIT_1);
	MC3630writeRegister8(hi2c, MC3630_REG_MODE_C, value);
}

void MC3630setRangeCtrl(I2C_HandleTypeDef *hi2c, uint8_t range)
{
	uint8_t value;

	value = MC3630readRegister8(hi2c, MC3630_REG_RANGE_C);
	value &= 0b00000111;
	value |= (range << 4)&0x70;

	MC3630writeRegister8(hi2c, MC3630_REG_RANGE_C, value);
}

void MC3630setResolutionCtrl(I2C_HandleTypeDef *hi2c, uint8_t resolution)
{
	uint8_t value;

	value = MC3630readRegister8(hi2c, MC3630_REG_RANGE_C);
	value &= 0b01110000;
	value |= resolution;

	MC3630writeRegister8(hi2c, MC3630_REG_RANGE_C, value);
}

void MC3630setCWakeSampleRate(I2C_HandleTypeDef *hi2c, uint8_t sample_rate)
{
	uint8_t value;

	value = MC3630readRegister8(hi2c, MC3630_REG_RATE_1);
	value &= 0b00000000;
	value |= sample_rate;

	MC3630writeRegister8(hi2c, MC3630_REG_RATE_1, value);
}

void MC3630setSniffSampleRate(I2C_HandleTypeDef *hi2c, uint8_t sniff_sr)
{
	uint8_t value;

	value = MC3630readRegister8(hi2c, MC3630_REG_SNIFF_C);
	value &= 0b00000000;
	value |= sniff_sr;

	MC3630writeRegister8(hi2c, MC3630_REG_SNIFF_C, value);
}

void MC3630stop(I2C_HandleTypeDef *hi2c)
{
	MC3630setMode(hi2c, MC3630_STANDBY_MODE);
}

// Set Sniff threshold
void MC3630setSniffThreshold(I2C_HandleTypeDef *hi2c, uint8_t axis_cfg, uint8_t sniff_thr)
{
	uint8_t value;
	uint8_t regSniffAddr = 0x00;

	value = MC3630readRegister8(hi2c, MC3630_REG_SNIFFTH_C);

	switch(axis_cfg)
	{
	case MC36XX_AXIS_X: //Put X-axis to active
		regSniffAddr = 0x01;
		break;
	case MC36XX_AXIS_Y: //Put Y-axis to active
		regSniffAddr = 0x02;
		break;
	case MC36XX_AXIS_Z: //Put Z-axis to active
		regSniffAddr = 0x03;
		break;
	default:
		break;
	}

	MC3630writeRegister8(hi2c, MC3630_REG_SNIFFCF_C, regSniffAddr);
	value |= sniff_thr;
	MC3630writeRegister8(hi2c, MC3630_REG_SNIFFTH_C, value);
}

// Set Sniff detect counts, 1~62 events
void MC3630setSniffDetectCount(I2C_HandleTypeDef *hi2c, uint8_t axis_cfg, uint8_t sniff_cnt)
{
	uint8_t value;
	uint8_t sniff_cfg;
	uint8_t regSniffAddr = 0x00;

	sniff_cfg = MC3630readRegister8(hi2c, MC3630_REG_SNIFFCF_C);

	switch(axis_cfg)
	{
	case MC36XX_AXIS_X: //Select x detection count shadow register
		regSniffAddr = 0x05;
		break;
	case MC36XX_AXIS_Y: //Select y detection count shadow register
		regSniffAddr = 0x06;
		break;
	case MC36XX_AXIS_Z: //Select z detection count shadow register
		regSniffAddr = 0x07;
		break;
	default:
		break;
	}

	sniff_cfg |= regSniffAddr;
	MC3630writeRegister8(hi2c, MC3630_REG_SNIFFCF_C, sniff_cfg);

	value = MC3630readRegister8(hi2c, MC3630_REG_SNIFFTH_C);

	value |= sniff_cnt;
	MC3630writeRegister8(hi2c, MC3630_REG_SNIFFTH_C, value);

	sniff_cfg |= 0x08;
	MC3630writeRegister8(hi2c, MC3630_REG_SNIFFCF_C, sniff_cfg);
}

void MC3630setSniffAndOrN(I2C_HandleTypeDef *hi2c, uint8_t logicandor)
{
	uint8_t value;

	value = MC3630readRegister8(hi2c, MC3630_REG_SNIFFTH_C);

	switch(logicandor)
	{
	case MC36XX_ANDORN_OR:  //Axis or mode
		value &= 0xBF;
		break;
	case MC36XX_ANDORN_AND: //Axis and mode
		value |= 0x40;
		break;
	default:
		break;
	}

	MC3630writeRegister8(hi2c, MC3630_REG_SNIFFTH_C, value);
}

void MC3630setSniffDeltaMode(I2C_HandleTypeDef *hi2c, uint8_t deltamode)
{
	uint8_t value;

	value = MC3630readRegister8(hi2c, MC3630_REG_SNIFFTH_C);

    switch(deltamode)
    {
    case MC36XX_DELTA_MODE_C2P: //Axis C2P mode
        value &= 0x7F;
        break;
    case MC36XX_DELTA_MODE_C2B: //Axis C2B mode
        value |= 0x80;
        break;
    default:
        break;
    }

    MC3630writeRegister8(hi2c, MC3630_REG_SNIFFTH_C, value);
}

void MC3630setINTCtrl(I2C_HandleTypeDef *hi2c, uint8_t fifo_thr_int_ctl, uint8_t fifo_full_int_ctl, uint8_t fifo_empty_int_ctl, uint8_t acq_int_ctl, uint8_t wake_int_ctl)
{
	uint8_t value;

	MC3630setMode(hi2c, MC3630_STANDBY_MODE);

	value = (((fifo_thr_int_ctl & 0x01) << 6)
	           | ((fifo_full_int_ctl & 0x01) << 5)
	           | ((fifo_empty_int_ctl & 0x01) << 4)
	           | ((acq_int_ctl & 0x01) << 3)
	           | ((wake_int_ctl & 0x01) << 2)
	           | MC3630_INTR_C_IAH_ACTIVE_HIGH		//MC36XX_INTR_C_IAH_ACTIVE_LOW//
	           | MC3630_INTR_C_IIP_PUSH_PULL_MODE);	//MC36XX_INTR_C_IPP_MODE_OPEN_DRAIN)

	MC3630writeRegister8(hi2c, MC3630_REG_INTR_C, value);
}

void MC3630sniff(I2C_HandleTypeDef *hi2c)
{
	MC3630setMode(hi2c, MC3630_SNIFF_MODE);
}

void MC3630SetFIFOCtrl(I2C_HandleTypeDef *hi2c, uint8_t fifo_ctl, uint8_t fifo_mode, uint8_t fifo_thr)
{
	uint8_t value;

    if (fifo_thr > 31)	//maximum threshold
        fifo_thr = 31;

    MC3630setMode(hi2c, MC3630_STANDBY_MODE);

    value = ((fifo_ctl << 6) | (fifo_mode << 5) | fifo_thr);
    MC3630writeRegister8(hi2c, MC3630_REG_FIFO_C, value);
}

void MC3630wake(I2C_HandleTypeDef *hi2c)
{
	MC3630setMode(hi2c, MC3630_CWAKE_MODE);
}

void MC3630INThandler(I2C_HandleTypeDef *hi2c, MC36XX_interrupt_event_t *ptINT_Event)
{
	uint8_t value;

	value = MC3630readRegister8(hi2c, MC3630_REG_STATUS_2);

    ptINT_Event->bWAKE           = ((value >> 2) & 0x01);
    ptINT_Event->bACQ            = ((value >> 3) & 0x01);
    ptINT_Event->bFIFO_EMPTY     = ((value >> 4) & 0x01);
    ptINT_Event->bFIFO_FULL      = ((value >> 5) & 0x01);
    ptINT_Event->bFIFO_THRESHOLD = ((value >> 6) & 0x01);
    ptINT_Event->bSWAKE_SNIFF    = ((value >> 7) & 0x01);

    value &= 0x03;

    MC3630writeRegister8(hi2c, MC3630_REG_STATUS_2, value);
}

MC3630_AccelData MC3630_Read_ACC_Data(I2C_HandleTypeDef *hi2c)
{
	uint8_t raw_data[6];
	MC3630_AccelData accel_data;

	// Burst read from XOUT_LSB (0x02) through ZOUT_MSB (0x07)
	HAL_I2C_Mem_Read(hi2c, MC3630_I2C_ADDRESS, MC3630_REG_XOUT_LSB, I2C_MEMADD_SIZE_8BIT, raw_data, 6, 100);

	// Extract readings. Raw data is in the format: X LSB, X MSB, Y LSB, Y MSB, Z LSB, Z MSB
	accel_data.Xdata = (int8_t)raw_data[0];
	accel_data.Ydata = (int8_t)raw_data[2];
	accel_data.Zdata = (int8_t)raw_data[4];

	return accel_data;
}

