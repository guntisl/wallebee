// file_receiver.c

#include "file_receiver.h"
#include <string.h>  // For strlen and strcmp

extern UART_HandleTypeDef huart1;  // Ensure these are defined in your project
//extern CRC_HandleTypeDef hcrc;     // If using hardware CRC

void receiveFile(const char *fileType) {
    char startStr[50];
    snprintf(startStr, sizeof(startStr), "Receiving %s file. Send the file.\r\n", fileType);
    HAL_UART_Transmit(&huart1, (uint8_t*)startStr, strlen(startStr), HAL_MAX_DELAY);

    uint8_t fileBuffer[1024];
    uint32_t bytesReceived = 0;
    uint32_t flashAddress = FLASH_USER_START_ADDR;

    HAL_FLASH_Unlock();

    // Assume a function HAL_FLASHEx_Erase() is available
    if (HAL_FLASHEx_Erase(FLASH_USER_START_ADDR, FLASH_USER_END_ADDR) != HAL_OK) {
//        Error_Handler();
    }

    while (flashAddress < FLASH_USER_END_ADDR) {
        HAL_StatusTypeDef status = HAL_UART_Receive(&huart1, fileBuffer + bytesReceived, 1, HAL_MAX_DELAY);
        if (status == HAL_OK) {
            bytesReceived++;

            if (bytesReceived == sizeof(fileBuffer)) {
                uint8_t crc = crc8(fileBuffer, bytesReceived);
                uint8_t receivedCrc;
                HAL_UART_Receive(&huart1, &receivedCrc, 1, HAL_MAX_DELAY);
                if (crc == receivedCrc) {
                    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_QUADWORD, flashAddress, (uint32_t)fileBuffer) != HAL_OK) {
 //                       Error_Handler();
                    }
                    flashAddress += bytesReceived;
                    bytesReceived = 0;
                } else {
                    char errorStr[] = "CRC error. Transmission failed.\r\n";
                    HAL_UART_Transmit(&huart1, (uint8_t*)errorStr, strlen(errorStr), HAL_MAX_DELAY);
                    break;
                }
            }
        } else {
            break;
        }
    }

    if (strcmp(fileType, "firmware") == 0) {
        char completeStr[] = "Firmware received. Restarting...\r\n";
        HAL_UART_Transmit(&huart1, (uint8_t*)completeStr, strlen(completeStr), HAL_MAX_DELAY);
        NVIC_SystemReset();
    } else if (strcmp(fileType, "library") == 0) {
        char completeStr[] = "Library file received and stored.\r\n";
        HAL_UART_Transmit(&huart1, (uint8_t*)completeStr, strlen(completeStr), HAL_MAX_DELAY);
    }

    HAL_FLASH_Lock();
}

