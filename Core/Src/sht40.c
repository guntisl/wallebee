/*
 * sht40.c
 *
 *  Created on: 2023. gada 14. jūn.
 *      Author: oskars_selis
 */

#include <sht40.h>

HAL_StatusTypeDef SHT40Init(I2C_HandleTypeDef *hi2c)
{
	HAL_StatusTypeDef status = HAL_OK;
	uint8_t tx_data = SHT40_SERIAL_NUMBER;
	status = HAL_I2C_Master_Transmit(hi2c, SHT40_I2C_ADDRESS, &tx_data, 1, HAL_MAX_DELAY);
	if (status != HAL_OK)
	{
		return HAL_ERROR;
	}
	HAL_Delay(15);
	uint8_t rx_data[6];
	status = HAL_I2C_Master_Receive(hi2c, SHT40_I2C_ADDRESS, rx_data, 6, HAL_MAX_DELAY);
	if (status != HAL_OK)
	{
		return HAL_ERROR;
	}
	return status;
}

float SHT40_measure_temperature(I2C_HandleTypeDef *hi2c)
{
	uint8_t tx_data = SHT40_T_AND_RH_HIGH;

	HAL_I2C_Master_Transmit(hi2c, SHT40_I2C_ADDRESS, &tx_data, 1, HAL_MAX_DELAY);

	HAL_Delay(15);

	uint8_t rx_data[6];
	HAL_I2C_Master_Receive(hi2c, SHT40_I2C_ADDRESS, rx_data, 6, HAL_MAX_DELAY);
	float t_ticks = 0;
	t_ticks = (uint16_t)rx_data[0] * 256 + (uint16_t)rx_data[1];
	float _temperature = -45 + 175 * t_ticks / 65535;

	return _temperature;
}



float SHT40_measure_humidity(I2C_HandleTypeDef *hi2c)
{
	uint8_t tx_data = SHT40_T_AND_RH_HIGH;
	HAL_I2C_Master_Transmit(hi2c, SHT40_I2C_ADDRESS, &tx_data, 1, HAL_MAX_DELAY);

	HAL_Delay(15);

	uint8_t rx_data[6];
	HAL_I2C_Master_Receive(hi2c, SHT40_I2C_ADDRESS, rx_data, 6, HAL_MAX_DELAY);

	float rh_ticks = (uint16_t)rx_data[3] * 256 + (uint16_t)rx_data[4];

	float _humidity = -6 + 125 * rh_ticks / 65535;
	_humidity = fmin(fmax(_humidity, (float)0.0), (float)100.0);

	return _humidity;
}
