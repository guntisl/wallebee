// file_receiver.h

#ifndef FILE_RECEIVER_H
#define FILE_RECEIVER_H

#include "stm32u5xx_hal.h"  // Include HAL header for STM32U5

// Define FLASH start and end addresses (adjust as needed)
#define FLASH_USER_START_ADDR   0x08000000
#define FLASH_USER_END_ADDR     0x08008000

//void receiveFile(const char *fileType);

uint8_t crc8(const uint8_t *data, size_t length); 
#endif // FILE_RECEIVER_H
