#ifndef DS18B20_H
#define DS18B20_H

#include "stm32u5xx_hal.h"

// Define sensor identifiers
typedef enum {
    SENSOR_1 = 0,
    SENSOR_2,
    SENSOR_3,
    SENSOR_4,
    SENSOR_5,
    SENSOR_6,
    SENSOR_7,
    SENSOR_8
} DS18B20_Sensor_t;

// Function prototypes
void DS18B20_Init(void);
float DS18B20_ReadTemp(DS18B20_Sensor_t sensor);

#endif // DS18B20_H

