/*
 * sht40.h
 *
 *  Created on: 2023. gada 14. jūn.
 *      Author: oskars_selis
 */

#ifndef INC_SHT40_H_
#define INC_SHT40_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stm32u5xx_hal.h>
#include <math.h>

extern float temperatureData;
extern float humidityData;

// Registers
#define SHT40_I2C_ADDRESS			(0x44 << 1)			// 7-bit I2C address of the SHT40

#define SHT40_T_AND_RH_HIGH			0xFD				// Measure T & RH with high precision
#define SHT40_T_AND_RH_MEDIUM		0xF6				// Measure T & RH with medium precision
#define SHT40_T_AND_RH_LOW			0xE0				// Measure T & RH with lowest precision

#define SHT40_SERIAL_NUMBER			0x89				// Read serial number

#define SHT40_SOFT_RESET			0x94

#define SHT40_AH_200MW_1S			0x39				// Activate heater with 200mW for 1s, including a high precision measurement just before deactivation
#define SHT40_AH_200MW_0_1S			0x32				// Activate heater with 200mW for 0.1s including a high precision measurement just before deactivation
#define SHT40_AH_110MW_1S			0x2F				// Activate heater with 110mW for 1s including a high precision measurement just before deactivation
#define SHT40_AH_110MW_0_1S			0x24				// Activate heater with 110mW for 0.1s including a high precision measurement just before deactivation
#define SHT40_AH_20MW_1S			0x1E				// Activate heater with 20mW for 1s including a high precision measurement just before deactivation
#define SHT40_AH_20MW_0_1S			0x15				// Activate heater with 20mW for 0.1s including a high precision measurement just before deactivation

HAL_StatusTypeDef SHT40Init(I2C_HandleTypeDef *hi2c);
float SHT40_measure_temperature(I2C_HandleTypeDef *hi2c);
float SHT40_measure_humidity(I2C_HandleTypeDef *hi2c);

#ifdef __cplusplus
}
#endif

#endif /* INC_SHT40_H_ */
