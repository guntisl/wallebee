// power.h

#ifndef POWER_H
#define POWER_H

#include "stm32u5xx_hal.h"
#include "rtc_handler.h" // Include the RTC header if needed

void enterStopMode(void);
void enterStandbyMode(void);
void sleepWithWakeupSources(void);

#endif // POWER_H
