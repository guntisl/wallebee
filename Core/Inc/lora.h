// lora.h

#ifndef LORA_H
#define LORA_H

#include "stm32u5xx_hal.h"
#define LORA_DATE_SIZE 10
#define LORA_TIME_SIZE 10


#define APP_EUI "2CF7F1203230DEB5"
#define DEV_EUI "70B3D57ED005F950"
#define APPS_S_KEY "00B03209501003D0DA08E0B605403344"
#define NW_KEY "B973E4E86ED083B5D582057B0E14A19E"


// Define message structure
typedef struct {
    char Vbatt[1];
    float temp1;
    float temp2;
    float temp3;
    float humidity;
    uint8_t broodingState;
    int8_t broodingQuality;
    uint8_t incomingState;
    uint8_t swarmingState;
    uint8_t hungerState;
    uint8_t queenlessState;
    uint8_t hiveRate;
    uint8_t motionState;
    uint8_t frameState;
    uint8_t frameScript;
    uint8_t alarmState;
    uint8_t warningState;
    uint8_t userLedState;
    uint16_t amountOfDays;
} msg;

// External variables and functions
extern msg MESSAGE;
extern char loraDate[LORA_DATE_SIZE];
extern char loraTime[LORA_TIME_SIZE];

void E5ModuleMsg(const char *format, ...);
void E5ModuleTime(const char *format, ...);
void loraJoin(void);
void E5SendingMSG(msg MESSAGE);
void setPayloadAndSend(const msg *message);

void sendATCommand(const char *command);
#endif // LORA_H
