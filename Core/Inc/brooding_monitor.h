// brooding_monitor.h

#ifndef BROODING_MONITOR_H
#define BROODING_MONITOR_H

#include "stm32u5xx_hal.h"

typedef enum {
    BROODING_NONE,
    BROODING_WEAK,
    BROODING_NORMAL,
    BROODING_STRONG
} BroodingState;


BroodingState checkBroodingState(void);
void monitorBroodingQuality(void);

#endif // BROODING_MONITOR_H
