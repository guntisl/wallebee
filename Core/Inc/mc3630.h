/*
 * mc3630.h
 *
 *  Created on: 2023. gada 15. jūn.
 *      Author: oskars_selis
 */

#ifndef INC_MC3630_H_
#define INC_MC3630_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stm32u5xx_hal.h>

#define MC3630_I2C_ADDRESS			(0x4C << 1)		// 7-bit I2C address of the MC3630

// Registers
#define MC3630_REG_EXT_STAT_1		0x00			// Extended Status 1 (Read Only)
#define MC3630_REG_EXT_STAT_2		0x01			// Extended Status 2 (Read Only)
#define MC3630_REG_XOUT_LSB			0x02			// XOUT_LSB (Read Only)
#define MC3630_REG_XOUT_MSB			0x03			// XOUT_MSB (Read Only)
#define MC3630_REG_YOUT_LSB			0x04			// YOUT_LSB (Read Only)
#define MC3630_REG_YOUT_MSB			0x05			// YOUT_MSB (Read Only)
#define MC3630_REG_ZOUT_LSB			0x06			// ZOUT_LSB (Read Only)
#define MC3630_REG_ZOUT_MSB			0x07			// ZOUT_MSB (Read Only)
#define MC3630_REG_STATUS_1			0x08			// Status 1 (Read Only)
#define MC3630_REG_STATUS_2			0x09			// Status 2 (Read Only)

#define MC3630_REG_FREG_1			0x0D			// Feature 1 (Read and Write)
#define MC3630_REG_FREG_2			0x0E			// Feature 2 (Read and Write)
#define MC3630_REG_INIT_1			0x0F			// Initialization 1 (Write Only)
#define MC3630_REG_MODE_C			0x10			// Mode Control (Read and Write)
#define MC3630_REG_RATE_1			0x11			// Rate 1 (Read and Write)
#define MC3630_REG_SNIFF_C			0x12			// Sniff Control (Read and Write)
#define MC3630_REG_SNIFFTH_C		0x13			// Sniff Threshold Control (Shadow registers, accessible via a single register portal)
#define MC3630_REG_SNIFFCF_C		0x14			// Sniff Configuration (Read and Write)
#define MC3630_REG_RANGE_C			0x15			// Range Resolution Control (Read and Write)
#define MC3630_REG_FIFO_C			0x16			// FIFO Control (Read and Write)
#define MC3630_REG_INTR_C			0x17			// Interrupt Control (Read Only)
#define MC3630_REG_CHIP_ID			0x18			// Chip ID Register (Read Only)

#define MC3630_REG_INIT_3			0x1A			// Initialization 3
#define MC3630_REG_PMCR				0x1C			// Power Mode Control (Read and Write)

#define MC3630_REG_DMX				0x20			// Drive Motion X (Read and Write)
#define MC3630_REG_DMY				0x21			// Drive Motion Y (Read and Write)
#define MC3630_REG_DMZ				0x22			// Drive Motion Z (Read and Write)

#define MC3630_REG_RESET			0x24			// Reset (Read and Write)

#define MC3630_REG_INIT_2			0x28			// Initialization Register 2 (Read and Write)
#define MC3630_REG_TRIGC			0x29			// Trigger Count (Read and Write)
#define MC3630_REG_XOFFL			0x2A			// X-Offset LSB (Read and Write)
#define MC3630_REG_XOFFH			0x2B			// X-Offset MSB (Read and Write)
#define MC3630_REG_YOFFL			0x2C			// Y-Offset LSB (Read and Write)
#define MC3630_REG_YOFFH			0x2D			// Y-Offset MSB (Read and Write)
#define MC3630_REG_ZOFFL			0x2E			// Z-Offset LSB (Read and Write)
#define MC3630_REG_ZOFFH			0x2F			// Z-Offset MSB (Read and Write)
#define MC3630_REG_XGAIN			0x30			// X Gain (Read and Write)
#define MC3630_REG_YGAIN			0x31			// Y Gain (Read and Write)
#define MC3630_REG_ZGAIN			0x32			// Z Gain (Read and Write)

// Register values
#define MC3630_SLEEP_MODE			0x00
#define MC3630_STANDBY_MODE			0x01
#define MC3630_SNIFF_MODE			0x02
#define MC3630_CWAKE_MODE			0x05

#define MC3630_RESET				0x40			// Reset (or Power-On)in I2C
#define MC3630_I2C_ENABLE			0x40			// I2C mode enabled
#define MC3630_INIT_1				0x42
#define MC3630_INIT_DMX				0x01
#define MC3630_INIT_DMY				0x80
#define MC3630_INIT_2  				0x00
#define MC3630_INIT_3				0x00

// Bit Width of Accelerometer Data
#define MC3630_RES_6_BITS			0x00
#define MC3630_RES_7_BITS			0x01
#define MC3630_RES_8_BITS			0x02
#define MC3630_RES_10_BITS			0x03
#define MC3630_RES_12_BITS			0x04
#define MC3630_RES_14_BITS			0x05		// Only 12-bits if FIFO enabled

// Sample rate
#define MC36XX_CWAKE_SR_DEFAULT_54Hz 	0x00
#define MC36XX_CWAKE_SR_14Hz 			0x05
#define MC36XX_CWAKE_SR_28Hz 			0x06
#define MC36XX_CWAKE_SR_54Hz 			0x07
#define MC36XX_CWAKE_SR_105Hz 			0x08
#define MC36XX_CWAKE_SR_210Hz 			0x09
#define MC36XX_CWAKE_SR_400Hz 			0x0A
#define MC36XX_CWAKE_SR_600Hz 			0x0B

#define MC36XX_SNIFF_SR_105Hz       0x08

// G Range Selection
#define MC3630_RANGE_2G				(0x00 << 4)
#define MC3630_RANGE_4G				(0x01 << 4)
#define MC3630_RANGE_8G				(0x02 << 4)
#define MC3630_RANGE_16G			(0x03 << 4)
#define MC3630_RANGE_12G			(0x04 << 4)

// Interrupt handle
#define MC3630_INTR_C_IIP_OPEN_DRAIN_MODE	0x00
#define MC3630_INTR_C_IIP_PUSH_PULL_MODE	0x01

#define MC3630_INTR_C_IAH_ACTIVE_LOW		(0x00)
#define MC3630_INTR_C_IAH_ACTIVE_HIGH		(0x01 << 1)

#define MC3630_INTR_C_INT_WAKE				(0x01 << 2)
#define MC3630_INTR_C_INT_ACQ				(0x01 << 3)
#define MC3630_INTR_C_INT_FIFO_EMPTY		(0x01 << 4)
#define MC3630_INTR_C_INT_FIFO_FULL			(0x01 << 5)
#define MC3630_INTR_C_INT_FIFO_THRESH		(0x01 << 6)
#define MC3630_INTR_C_INT_SWAKE				(0x01 << 7)

// Axis
#define MC36XX_AXIS_X  0
#define MC36XX_AXIS_Y  1
#define MC36XX_AXIS_Z  2

// Sniff And Or N
#define MC36XX_ANDORN_OR  	0
#define MC36XX_ANDORN_AND  	1

// Sniff Delta Mode
#define MC36XX_DELTA_MODE_C2P  	0
#define MC36XX_DELTA_MODE_C2B  	1

#define MC36XX_FIFO_CTL_DISABLE 0
#define MC36XX_FIFO_CTL_ENABLE 	1
#define MC36XX_FIFO_CTL_END 	2

#define MC36XX_FIFO_MODE_NORMAL 	0
#define MC36XX_FIFO_MODE_WATERMARK 	1
#define MC36XX_FIFO_MODE_END		2

typedef struct {
	int8_t Xdata;
	int8_t Ydata;
	int8_t Zdata;
} MC3630_AccelData;

typedef struct {
	int16_t ext_status_1;
	int16_t ext_status_2;
	int16_t status_1;
	int16_t status_2;
} MC3630_AccStatus;

typedef struct
{
    unsigned char    bWAKE;
    unsigned char    bACQ;
    unsigned char    bFIFO_EMPTY;
    unsigned char    bFIFO_FULL;
    unsigned char    bFIFO_THRESHOLD;
    unsigned char    bRESV;
    unsigned char 	 bSWAKE_SNIFF;
    unsigned char    baPadding[2];
}   MC36XX_interrupt_event_t;


HAL_StatusTypeDef MC3630checkConnection(I2C_HandleTypeDef *hi2c);

uint8_t MC3630readRegisterBit(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t pos);
uint8_t MC3630readRegister8(I2C_HandleTypeDef *hi2c, uint8_t reg);
uint16_t MC3630readRegister16(I2C_HandleTypeDef *hi2c, uint8_t reg);
void MC3630readRegisters(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t *buffer, uint8_t len);
void MC3630writeRegisterBit(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t pos, uint8_t state);
void MC3630writeRegister8(I2C_HandleTypeDef *hi2c, uint8_t reg, uint8_t value);
void MC3630writeRegister16(I2C_HandleTypeDef *hi2c, uint8_t reg, int16_t value);

uint8_t MC3630start(I2C_HandleTypeDef *hi2c);
void MC3630init(I2C_HandleTypeDef *hi2c);
void MC3630setMode(I2C_HandleTypeDef *hi2c, uint8_t mode);
void MC3630setRangeCtrl(I2C_HandleTypeDef *hi2c, uint8_t range);
void MC3630setResolutionCtrl(I2C_HandleTypeDef *hi2c, uint8_t resolution);
void MC3630setCWakeSampleRate(I2C_HandleTypeDef *hi2c, uint8_t sample_rate);
void MC3630setSniffSampleRate(I2C_HandleTypeDef *hi2c, uint8_t sniff_sr);
void MC3630stop(I2C_HandleTypeDef *hi2c);
void MC3630setSniffThreshold(I2C_HandleTypeDef *hi2c, uint8_t axis_cfg, uint8_t sniff_thr);
void MC3630setSniffDetectCount(I2C_HandleTypeDef *hi2c, uint8_t axis_cfg, uint8_t sniff_cnt);
void MC3630setSniffAndOrN(I2C_HandleTypeDef *hi2c, uint8_t logicandor);
void MC3630setSniffDeltaMode(I2C_HandleTypeDef *hi2c, uint8_t deltamode);
void MC3630setINTCtrl(I2C_HandleTypeDef *hi2c, uint8_t fifo_thr_int_ctl, uint8_t fifo_full_int_ctl, uint8_t fifo_empty_int_ctl, uint8_t acq_int_ctl, uint8_t wake_int_ctl);
void MC3630sniff(I2C_HandleTypeDef *hi2c);
void MC3630SetFIFOCtrl(I2C_HandleTypeDef *hi2c, uint8_t fifo_ctl, uint8_t fifo_mode, uint8_t fifo_thr);
void MC3630wake(I2C_HandleTypeDef *hi2c);
void MC3630INThandler(I2C_HandleTypeDef *hi2c, MC36XX_interrupt_event_t *ptINT_Event);

MC3630_AccelData MC3630_Read_ACC_Data(I2C_HandleTypeDef *hi2c);

#ifdef __cplusplus
}
#endif

#endif /* INC_MC3630_H_ */
