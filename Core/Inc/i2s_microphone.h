// i2s_microphone.h

#ifndef I2S_MICROPHONE_H
#define I2S_MICROPHONE_H

#include "stm32u5xx_hal.h"

// Function prototypes
void Audio_Init(void);
void HAL_SAI_RxHalfCpltCallback(SAI_HandleTypeDef *hsai);
void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hsai);
//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);

void Process_AudioData(void);
#endif // I2S_MICROPHONE_H
