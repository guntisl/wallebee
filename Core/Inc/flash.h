// flash.h

#ifndef FLASH_H
#define FLASH_H

#include "stm32u5xx_hal.h"
#include <stddef.h>
#include <stdint.h>

#define FLASH_USER_START_ADDR   0x08000000  // Start address of user FLASH area
#define FLASH_USER_END_ADDR     (FLASH_USER_START_ADDR + 0x00400000 - 1)  // End address of user FLASH area (4 MB)
//#define FLASH_PAGE_SIZE         2048  // Adjust as necessary for the device

extern FLASH_EraseInitTypeDef EraseInitStruct;
extern uint32_t PageError;

uint8_t crc8(const uint8_t *data, size_t len);
void FLASH_Unlock(void);
void FLASH_Lock(void);
HAL_StatusTypeDef FLASH_Erase(uint32_t startAddress, uint32_t endAddress);
HAL_StatusTypeDef FLASH_Write(uint32_t address, uint8_t *data, uint32_t length);

#endif // FLASH_H
