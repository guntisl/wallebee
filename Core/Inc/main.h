/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32u5xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "flash.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/

/* USER CODE BEGIN Private defines */
#define ACC_INT_Pin GPIO_PIN_0
#define BTN_SAD_Pin GPIO_PIN_6
#define BTN_NEUTRAL_Pin GPIO_PIN_7
#define BTN_SMILE_Pin GPIO_PIN_12
#define BTN_WRN_Pin GPIO_PIN_11
//#define MICROSWITCH_1 GPIO_PIN_10
#define MICROSWITCH_2 GPIO_PIN_14
#define BTN_DEBOUNCE_TIME 200

#define LED_POWER_Pin GPIO_PIN_4
#define LED_POWER_Port GPIOC

#define LED_USER_Pin GPIO_PIN_2
#define LED_USER_Port GPIOE

#define LED_WARNING_Pin GPIO_PIN_5
#define LED_WARNING_Port GPIOC

#define LED_NEUTRAL_Pin GPIO_PIN_7
#define LED_NEUTRAL_Port GPIOC

#define LED_SMILE_Pin GPIO_PIN_11
#define LED_SMILE_Port GPIOA

#define LED_SAD_Pin GPIO_PIN_8
#define LED_SAD_Port GPIOD
#define ON 0
#define OFF 1

enum LedNames {
	ledSad,
	ledNeutral,
	ledSmile,
	ledUser,
	ledPower,
	ledWarning,

};




extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
