/*
 * cat24m01.h
 *
 *  Created on: 2023. gada 21. jūn.
 *      Author: oskars_selis
 */

#ifndef INC_CAT24M01_H_
#define INC_CAT24M01_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stm32u5xx_hal.h>


#define EEPROM_I2C_ADDRESS_1	0xA0			// 8-bit address, A2 and A1 connected to the ground a16=0
#define EEPROM_I2C_ADDRESS_2	0xA2			// 8-bit address, A2 and A1 connected to the ground a16=1

#define EEPROM_PAGE_SIZE		256				// Page size in bytes
#define EEPROM_SIZE				128 * 1024 		// Total memory size in bytes
#define I2C_TIMEOUT				100				// Timeout for I2C operations in ms


HAL_StatusTypeDef EEPROM_Init(I2C_HandleTypeDef *hi2c);
HAL_StatusTypeDef EEPROM_Write(I2C_HandleTypeDef *hi2c, uint16_t memAddress, uint8_t *pData, uint16_t size, uint8_t pageSelect);
HAL_StatusTypeDef EEPROM_Read(I2C_HandleTypeDef *hi2c, uint16_t memAddress, uint8_t *pData, uint16_t size, uint8_t pageSelect);


#ifdef __cplusplus
}
#endif

#endif /* INC_CAT24M01_H_ */
