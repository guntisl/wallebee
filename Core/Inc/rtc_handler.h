// rtc_handler.h

#ifndef RTC_HANDLER_H
#define RTC_HANDLER_H

#include "stm32u5xx_hal.h"

void initRTC(void);
void configureRTCForDayTime(void);
void configureRTCForNightTime(void);
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc);
uint32_t getCurrentDay(void);

#endif // RTC_HANDLER_H
