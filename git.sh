#!/bin/bash

# Check if a commit message was provided
if [ -z "$1" ]; then
  echo "Usage: ./git.sh <commit message>"
  exit 1
fi

# Combine all arguments into a single commit message
commit_message="$*"

# Run Git commands
git add .
git commit -m "$commit_message"
git push
